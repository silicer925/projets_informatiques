"""
ce fichier a pour but de trouver les générateurs des CE sur Z/NZ

"""

"""
somme sur les CE : 
cf image "somme_sur_CE.JPG"

"""
#
p = (8,17)
q = (14,19)
p_bis = (6,8)
q_bis = (10,23)
q_ter = (20,34)

test_p = (6,8)
test_q = (10,36)

#a est un param de la CE  y^2 = x^3 + ax + b
#fonctionne

def bezout(a : int, b : int):
    if b == 0:
        return 1,0
    else : 
        u,v = bezout(b, a%b)
        return v, (u-(a//b)*v)
    
def somme(p,q , n : int, a : int):
    x_1,y_1 = p
    x_2, y_2 = q
    if(x_1 == x_2 ==y_1 == y_2):
        #cas de deux points à l'infini
        return (0,0)
    if(x_1 == y_1 == 0):
        return(x_2, y_2)
    if(x_2 == y_2 == 0):
        return (x_1, y_1)
    if(x_1 == x_2 & y_1 == (n - y_2)):
        return (0,0)
    if(x_1 != x_2 or y_1 != y_2):
        intermediaire = (y_2 - y_1) * (bezout(x_2 - x_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    else:
        #cas où on fait 2P
        # avec P = (x_1,x_2), x_2 != 0, sinon on est dans le cas 4)
        intermediaire = (3 * x_1**2 + a) * (bezout(2*y_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3


"""
G : le point de la CE
n : quand on veut renvoyer n*G
ordre : l'ordre du groupe Z/nZ
coos : coos de la CE

"""

def expo_rapide_CE(G,n, ordre, coos):
    p = (0,0) #point à l'infini, convention personnelle
    if(n == 0):
        return p
    if(n<0):
        n_0 = -n
        q = (-G[0], -G[1])
    else:
        n_0 = n
        q = G
    while(True):
        if(n_0%2 == 1):
            p = somme(q,p,ordre,coos[0])
        n_0 = n_0//2
        if(n_0 ==0):
            return p
        q = somme(q,q,ordre,coos[0])

x = expo_rapide_CE((41,92), 11, 101, (1,1))
moins_x = ((-x[0])%101, -x[1]%101 )
print(x)
var = (0,0)
for i in range(11):
    var = somme(var, (41,92), 101, 1)
print("var = " + str(var))

print(moins_x)
print(expo_rapide_CE((41,92), -11, 101, (1,1)))
print(somme((76,39), expo_rapide_CE((41,92), -11, 101, (1,1)), 101, 1))
print(somme((76,39), moins_x, 101, 1))




"""
print(somme(test_p,test_q, 59))
print(somme(p,q, 59))
"""



