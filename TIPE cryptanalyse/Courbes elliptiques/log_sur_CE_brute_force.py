"""
On représente les points par des couples (x,y) qui sont leurs coordonnées



NB : on définit le point à l'infini par (0,0)

"""


"""
on a l'exemple suivant (cd doc 7):
E : y^2 = x^3 + x + 1 sur F_q avec q = 101
groupe cyclque, isomorphe à Z/105Z
générateur P : [42: 92: 1]

point Q : [76:39:1] -> résultat : 10



"""

p = (8,17)
q = (14,19)
p_bis = (6,8)
q_bis = (10,23)
q_ter = (20,34)

test_p = (6,8)
test_q = (10,36)

def bezout(a : int, b : int):
    if b == 0:
        return 1,0
    else : 
        u,v = bezout(b, a%b)
        return v, (u-(a//b)*v)
    
print(bezout(4,59))


#a est un param de la CE  y^2 = x^3 + ax + b
#fonctionne
def somme(p,q , n : int, a : int):
    x_1,y_1 = p
    x_2, y_2 = q
    #print(x_1, y_1)
    #print(x_2, y_2)
    if(x_1 == x_2 ==y_1 == y_2):
        #cas de deux points à l'infini
        return (0,0)
    if(x_1 == y_1 == 0):
        return(x_2, y_2)
    if(x_2 == y_2 == 0):
        return (x_1, y_1)
    if(x_1 == x_2 and y_1 == (n - y_2)):
        return (0,0)
    if(x_1 != x_2 or y_1 != y_2):
        
        intermediaire = (y_2 - y_1) * (bezout(x_2 - x_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    else:
        #cas où on fait 2P
        # avec P = (x_1,x_2), x_2 != 0, sinon on est dans le cas 4)
        intermediaire = (3 * x_1**2 + a) * (bezout(2*y_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3

#print(somme(p,q,59))
#print(somme(p_bis, q_bis, 59))
#print(somme(test_p, test_q, 59))

#print(somme((41,92),(41,92), 101,1))
print(somme((64, 35), (41, 92), 101, 1))
print(somme((41,92),(64,35), 101,1))


def expo_rapide_CE(G,n, ordre, coos):
    p = (0,0) #point à l'infini, convention personnelle
    if(n == 0):
        return p
    if(n<0):
        n_0 = -n
        q = (-G[0], -G[1])
    else:
        n_0 = n
        q = G
    while(True):
        if(n_0%2 == 1):
            p = somme(q,p,ordre,coos[0])
        n_0 = n_0//2
        if(n_0 ==0):
            return p
        q = somme(q,q,ordre,coos[0])




"""
algo brute force à proprement parlé : 
on a P un générateur de la courbe elliptique sur Z/pZ

"""



#note : ce = (a,b) pour la CE  y^2 = x^3 + ax + b
def brute_force(coos, g,y, n):
    #print("debut fct")
    puiss = 1
    point_en_cours = g
    for i in range(1,n):
        #print(point_en_cours)
        if point_en_cours == y : 
            #print("fin brute_force")
            return puiss
        else :
            point_en_cours = somme(point_en_cours,g,n,coos[0])
            puiss += 1 
    #print("fin_brute_force")
    return "cette équation n'a pas de solution"

"""
print("brute_un ", end = " ")
print(brute_force((1,1), (41,92), (76,39), 101))

print(brute_force((1,1), (41,92), (41,92), 101))

print(expo_rapide_CE((41,92), 65, 101, (1,1)))

print(expo_rapide_CE((41,92), 10, 101, (1,1)))


point = (41,92)
for i in range(1,65):
    point = somme(point, (41,92), 101, 1)
print("point : ", end = " ")
print(point)
"""


import timeit

def lit_et_resout_brute_force(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    targ = open(target, "a")
    i = 0
    tab = ["","","","","","","",""]
    for ligne in f:
        if( i != 8 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "brute_force( (" + tab[1] + "," + tab[2] + "),(" +  tab[3] + ", " + tab[4] + "),(" + tab[6] + ", " + tab[7] + "), " + tab[0] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import brute_force" )
            valeur = test.timeit(1)
            dico_temps[int((tab[0])[:-1])] = valeur
            targ.write((tab[0])[:-1] + "\n")
            targ.write(str(valeur) + "\n")
            i = 0
    f.close()
    targ.close()
    return dico_temps

lit_et_resout_brute_force("CE_pb.txt", "CE_temps_brute_force.txt")


    