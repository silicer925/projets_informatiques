import random

def bezout(a : int, b : int):
    if b == 0:
        return 1,0
    else : 
        u,v = bezout(b, a%b)
        return v, (u-(a//b)*v)
    
#print(bezout(4,59))


#a est un param de la CE  y^2 = x^3 + ax + b
#fonctionne
def somme(p,q , n : int, a : int):
    x_1,y_1 = p
    x_2, y_2 = q
    #print(x_1, y_1)
    #print(x_2, y_2)
    if(x_1 == x_2 ==y_1 == y_2):
        #cas de deux points à l'infini
        return (0,0)
    if(x_1 == y_1 == 0):
        return(x_2, y_2)
    if(x_2 == y_2 == 0):
        return (x_1, y_1)
    if(x_1 == x_2 and y_1 == (n-1 - y_2)):
        return (0,0)
    if(x_1 != x_2 or y_1 != y_2):
        
        intermediaire = (y_2 - y_1) * (bezout(x_2 - x_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    else:
        #cas où on fait 2P
        # avec P = (x_1,x_2), x_2 != 0, sinon on est dans le cas 4)
        intermediaire = (3 * x_1**2 + a) * (bezout(2*y_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3



def trouve_rac_brute_force(p : int, y_carre : int):
    x_0 = 1
    for i in range(p-1):
        if((i * i) %p == y_carre):
            return i
    return -1
    

def cree_instance(p : int):
    reussi = False
    while(not(reussi)):
        a = random.randint(-10,10)
        b = random.randint(-10,10)
        x_0 = random.randint(0,p-1)
        y_carre = (x_0*x_0*x_0 + a * x_0 + b) %p
        residu_quadra = (y_carre**((p-1)//2) ) % p
        if(residu_quadra == 1): #(si admet une rac carrée)
            if(p % 4 == 3): #cas nombre de Blum
                y_0 = (y_carre**((p+1)/4)) % p
                reussi = True
                
            else: #cas ou p%4 = 1 
                #print("bla")
                y_0 = trouve_rac_brute_force(p,y_carre)
                if (y_0 != -1):
                    reussi = True
                
    l = random.randint(1,p)
    point = (x_0, y_0)
    for i in range(l):
        point = somme(point, (x_0,y_0), p, a)
    return [p,a,b,x_0,y_0,l,point[0], point[1]]



def cree_pb(fichier : str, target : str):
    fic = open(fichier, "r")
    for line in fic:
        t = open(target,"a")
        p = int(line)
        pb = cree_instance(p)
        for i in range (len(pb)):
            t.write(str(pb[i]) + "\n")
        t.write("e\n")
        t.close()
        print("un de plus.")
    return 1

cree_pb("CE_test.txt", "CE_pb.txt")
        