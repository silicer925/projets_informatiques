"""
Ne fonctionne pas du tout du tout du tout du tout

attention : la some a été modifiée, ainsi que l'expo rapide pour fct    avec les moins


calcul du lagarithme discret sur les CE de type y^3 = x^2 + ax + b sur un corps finis F_p*

entrées : idem que brute force : 
-elément y de la CE
-g un générateur de la CE
-n l'ordre de la CE

Sortie : l de [|1;n|] tq g*l = y

Principe :
-calculer m = partie entière supérieure (sqrt(n))
-creer un dictionnaire qui contient (g*i : i) pour 0<=i<m
-initialiser x = y et  j = 0
-boucle : tant que x n'es pas un clef de D : 
    x = x * g^(-m)
    j = j +1
-retourner jm + D[x] (D est le dictionnaire)


"""




import math


def bezout(a : int, b : int):
    if b == 0:
        return 1,0
    else : 
        u,v = bezout(b, a%b)
        return v, (u-(a//b)*v)
    



def somme(p,q , n : int, a : int):
    x_1,y_1 = p
    x_2, y_2 = q
    #print(x_1, y_1)
    #print(x_2, y_2)
    if(x_1 == x_2 ==y_1 == y_2):
        #cas de deux points à l'infini
        return (0,0)
    if(x_1 == y_1 == 0):
        return(x_2, y_2)
    if(x_2 == y_2 == 0):
        return (x_1, y_1)
    if(x_1 == x_2 and y_1 == (n-1 - y_2)):
        return (0,0)
    if(x_1 != x_2 or y_1 != y_2):
        
        intermediaire = (y_2 - y_1) * (bezout(x_2 - x_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    else:
        #cas où on fait 2P
        # avec P = (x_1,x_2), x_2 != 0, sinon on est dans le cas 4)
        intermediaire = (3 * x_1**2 + a) * (bezout(2*y_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    

"""
G : le point de la CE
n : quand on veut renvoyer n*G
ordre : l'ordre du groupe Z/nZ
coos : coos de la CE

"""

def expo_rapide_CE(G,n, ordre, coos):
    p = (0,0) #point à l'infini, convention personnelle
    if(n == 0):
        return p
    if(n<0):
        n_0 = -n
        q = (G[0], ordre-1-G[1])
    else:
        n_0 = n
        q = G
    while(True):
        if(n_0%2 == 1):
            p = somme(q,p,ordre,coos[0])
        n_0 = n_0//2
        if(n_0 ==0):
            return p
        q = somme(q,q,ordre,coos[0])

def log_pas_de_bebe_pas_de_geant_CE( coos,g,y,n) : 
    m = math.ceil(math.sqrt(n)) 
    print(m)
    D = {}
    somme_de_g = (0,0)
    #pas de bébés
    for i in range (0,m) : 
        D[somme_de_g] = i
        somme_de_g = somme(somme_de_g, g, n, coos[0])
        #puiss_de_g = (puiss_de_g*g)%n
    print(D)

    x = y
    j = 0
    print(x)
    #pas de géants
    cpt = 0
    point = (0,0)
    for i in range(m):
        point = somme(point, g,n,coos[0])
    #ici point faut mP=mg
    while x not in D.keys() :
        j = j + 1
        # ici g^(-m) est (l'inverse de g)^m dans Z/nZ,*
        #x = (x * g^m)%n
        x = somme(x,(point[0], -point[1]),n,coos[0])
        print(x)
        cpt += 1
        
    res = D[x]

    """
    NB : on est ici obligé de faire un truc itératif, car dans l'exemple ou 
    on est à un étape égal à 0, si c'est la dernière (ie on fait le modulo direct)
    alors ça va, sinon le résultat récupère un +1 en trop
    (cf exemple pour D[x] = 6, m = 6, cpt = 3)
    """
    
    
    return res + j*m

"""
print("res : " + str(log_pas_de_bebe_pas_de_geants_CE((76,39), (41,92), 101, (1,1))))
#print(somme((64, 35), (41, 92), 101, 1))
#print(somme((41,92),(64,35), 101,1))

print("tests_divers_et_variés")
moins_jmp = expo_rapide_CE((41,92),(-1)*11,101,(1,1) )
print(somme((76,39), moins_jmp, 101,1))

print(expo_rapide_CE((41,92), (-1), 101, (1,1)))
print(somme((41, 92), expo_rapide_CE((41,92), (-1), 101, (1,1)),101, 1))



moins = expo_rapide_CE((76,39), (-1), 101, (1,1))
moins_jmp_calc = somme((82,71), moins, 101, 1)
print(moins_jmp_calc)

print(somme((76,39),moins_jmp_calc, 101, 1))


#test pour l'expo rapide négative : 

p = (76,39)
moins_p = expo_rapide_CE((76, 39), (-1), 101, (1,1))
print(somme(p, moins_p, 101, 1))

"""

import timeit

def lit_et_resout_BSGS(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    
    i = 0
    tab = ["","","","","","","",""]
    for ligne in f:
        if( i != 8 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "log_pas_de_bebe_pas_de_geant_CE( (" + tab[1] + "," + tab[2] + "),(" +  tab[3] + ", " + tab[4] + "),(" + tab[6] + ", " + tab[7] + "), " + tab[0] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import log_pas_de_bebe_pas_de_geant_CE" )
            valeur = test.timeit(1)
            dico_temps[int((tab[2])[:-1])] = valeur
            targ = open(target, "a")
            targ.write((tab[2])[:-1] + "\n")
            targ.write(str(valeur) + "\n")
            targ.close()
            i = 0
    f.close()
    
    return dico_temps

lit_et_resout_BSGS("CE_pb.txt", "CE_temps_BSGS.txt")
