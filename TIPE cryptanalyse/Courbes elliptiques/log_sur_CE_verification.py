import math


def bezout(a : int, b : int):
    if b == 0:
        return 1,0
    else : 
        u,v = bezout(b, a%b)
        return v, (u-(a//b)*v)

def somme(p,q , n : int, a : int):
    x_1,y_1 = p
    x_2, y_2 = q
    #print(x_1, y_1)
    #print(x_2, y_2)
    if(x_1 == x_2 ==y_1 == y_2):
        #cas de deux points à l'infini
        return (0,0)
    if(x_1 == y_1 == 0):
        return(x_2, y_2)
    if(x_2 == y_2 == 0):
        return (x_1, y_1)
    if(x_1 == x_2 and y_1 == (n-1 - y_2)):
        return (0,0)
    if(x_1 != x_2 or y_1 != y_2):
        
        intermediaire = (y_2 - y_1) * (bezout(x_2 - x_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    else:
        #cas où on fait 2P
        # avec P = (x_1,x_2), x_2 != 0, sinon on est dans le cas 4)
        intermediaire = (3 * x_1**2 + a) * (bezout(2*y_1, n)[0])
        x_3 = (intermediaire **2 -x_1 - x_2)%n
        y_3 = (-y_1 + intermediaire * (x_1 - x_3) )%n
        return x_3, y_3
    


    
def verification(y, g, p, res : int, a : int):
    nombre = g
    for i in range(res-1):
        nombre = somme( nombre, g, p, a)
    print(nombre)
    return (nombre == y)

print(verification((76,39),(41,92),101, 65, 1))