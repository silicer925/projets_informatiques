"""
algo : 
entree : p premier impair
sortie : g génrateur de Z_p*



"""
import random
import math

def pgcd(a : int, b : int):
    if (a == 0):
        return b
    if (b == 0):
        return a
    if (a == 1 or b == 1):
        return 1
    else:
        if (a >= b):
            return pgcd(b, a%b)
        else:
            return pgcd(a, b%a)

def decomp_nb_premiers(nb : int) :
    diviseur = {} 
    for i in range (2, math.floor(math.sqrt(nb))):
        if(nb%i == 0):
            j = 1
            while(nb% (i**j) == 0):
                j += 1
        diviseur[i] = j
    return diviseur




def ordre(a: int, p : int):
    a_bis = a
    cpt = 1
    while(a_bis != 1):
        a_bis = (a_bis * a) % p
        cpt += 1
    return cpt

def est_dans_gen_a(b: int,a : int, p : int):
    cpt = 1
    puiss_a = a
    while (cpt<p and puiss_a != b) :
        puiss_a = (puiss_a * a )%p
        cpt += 1
    return cpt<p 

def calcul_u_v(pgcd :int, s : int, t : int):
    u_prime = s//pgcd
    v_prime = t//pgcd
    diviseur = decomp_nb_premiers(pgcd)
    for elt in diviseur.keys():
        if(u_prime % elt):
            u_prime *= elt**diviseur[elt]
        else:
            v_prime *= elt** diviseur[elt]
    return(u_prime,v_prime)



def algo_gauss(p : int):
    #initialisation
    a = random.randint(1,p-1)
    s = ordre(a,p)
    while(s != p-1):
        b = random.randint(1,p-1)
        print(b)
        if( not(est_dans_gen_a(b,a,p))):
            print("bla")
            t = ordre(b,p)
            #determiner u,v de N* tq u|s, v|t, v^u  1 et uv = ppcm(s,t)
            u,v = calcul_u_v(pgcd(s,t), s,t)
            print("uv : ", end = " ")
            print(u,v)
            a = (a**(s//u) * b**(t//v))%p
            s = ordre(a,p)
    return a

print("resultat :   " + str(algo_gauss(127)))
