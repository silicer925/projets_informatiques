"""
nombre de premiers deja fait : 0 à 0




"""


import random
def calcul_inverse_euclide_etendu(g,n):
    if (n == 0):
        return (g,1, 0)
    else:
        (dbis, ubis, vbis) = calcul_inverse_euclide_etendu(n, g%n)
        return (dbis, vbis, ubis - (g//n) * vbis)
    

def casse_log_discret_additif(y,g,n):
    inv_de_g = calcul_inverse_euclide_etendu(g,n)[1]
    return (y*inv_de_g %n)



def genere_pb(p : int):
    g = random.randint(1,p-1)
    l = random.randint(1,p-1)
    return (g, (l*g)%p, p, l)

def genere_plein_pb_depuis_fichier(fichier : str, target : str) :
    f = open(fichier, "r")
    t = open(target, "a")
    premiers = f.readlines()
    for nb in premiers:
        probleme = genere_pb(int(nb))
        for i in range(0,4):
            t.write(str(probleme[i]) + "\n")
        t.write("e\n")
    f.close()
    t.close()


import timeit

def lit_et_resout_additif(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    targ = open(target, "a")
    i = 0
    tab = ["","","",""]
    for ligne in f:
        if( i != 4 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "casse_log_discret_additif(" + tab[1] + "," +  tab[0] + "," + tab[2] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import casse_log_discret_additif" )
            valeur = test.timeit(1)
            dico_temps[int((tab[2])[:-1])] = valeur
            targ.write((tab[2])[:-1] + "\n")
            targ.write(str(valeur) + "\n")
            i = 0
    f.close()
    targ.close()
    return dico_temps




#genere_plein_pb_depuis_fichier("fichier.txt", "pbs_additifs.txt")
print(lit_et_resout_additif("pbs_additifs.txt", "temps_additif.txt"))
