# on va ici calculer le logarithme discret sur (Z/nZ,+)
"""
vérifier que cela fonctionne
"""



'''
Calculer le logarithme discret, c'est:
entrées : g générateur, n ordre du groupe et y
sorties : a tq y = g*a


Sur (Z/nZ,+), le problème du log discret revient à faire une division dans (Z/nZ)

Note : pour trouver ce résultat de division, il suffit d'utiliser l'algo d'euclide étendu
car les générateurs de (Z/nZ,+) sont exactement les g premiers avec n
Et sachant que g est par définition générateur, c'est ok

ainsi on a y = g*a
et on obtient g^-1 par euclide étendu,
on muliplie y par g^-1 et on a y*g^-1 = a*g*g^-1 = a
et ce en cpx raisonnable


'''

"""
renvoie (pgcd(g,n), u, v) tq gu + vn = r (v est l'inverse de g)
"""
def calcul_inverse_euclide_etendu(g,n):
    if (n == 0):
        return (g,1, 0)
    else:
        (dbis, ubis, vbis) = calcul_inverse_euclide_etendu(n, g%n)
        return (dbis, vbis, ubis - (g//n) * vbis)
    

def casse_log_discret_additif(g,y,n):
    inv_de_g = calcul_inverse_euclide_etendu(g,n)[1]
    return (y*inv_de_g %n)

    

