"""
la fonction genere_pb fonctionne sans soucis



"""


import random

def genere_pb(p : int):
    g = random.randint(1, p-1)
    l = random.randint(1,p-1)
    #print("g : " + str(g) + "   l : " + str(l))
    #print("g puissance l : " + str (g**l))
    return (g, (g**l) % p, p, l)


#print(genere_pb(5))

def genere_plein_pb_depuis_fichier(fichier : str, target : str):
    f = open(fichier, "r")
    
    premiers = f.readlines()
    for elt in premiers :
        t = open(target, "a")
        probleme = genere_pb(int(elt))
        for i in range(0,4):
            t.write(str(probleme[i]) + "\n")
        t.write("e\n")
        t.close()
        print("un de plus")
   
    f.close()

genere_plein_pb_depuis_fichier("tres_grands_nbs_premiers.txt", "fichier_pb_Z_nZ_grand_bis.txt")
