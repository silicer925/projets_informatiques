"""
prend un fichier en entrée (de type pb Z_nZ) et resout les pb avec l'algo souhaité

"""
def casse_log_discret( y,g,n) :
    if(y == 1):
        return 0
    
    val = g
    puiss = 1
    for i in range (1,n) : 
        if val == y : 
            return puiss
        else :
            val = (val * g) % n
            puiss += 1
    return "cette équation n'a pas de solution"


import timeit

def lit_et_resout_brute_force(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    targ = open(target, "a")
    i = 0
    tab = ["","","",""]
    for ligne in f:
        if( i != 4 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "casse_log_discret(" + tab[1] + "," +  tab[0] + "," + tab[2] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import casse_log_discret" )
            valeur = test.timeit(1)
            dico_temps[int((tab[2])[:-1])] = valeur
            targ.write((tab[2])[:-1] + "\n")
            targ.write(str(valeur) + "\n")
            i = 0
    f.close()
    targ.close()
    return dico_temps

print(lit_et_resout_brute_force("fichier_pb_Z_nZ.txt", "temps_brute_force_bis.txt"))
