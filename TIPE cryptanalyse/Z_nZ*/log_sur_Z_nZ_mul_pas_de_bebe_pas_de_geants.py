#état : fonctionne

"""


calcul du lagarithme discret sur un groupe multiplicatif 
type Z/nZ

entrées : idem que brute force : 
-elément y de G
-g un générateur de G
-n l'ordre de G

Sortie : l de Z/nZ tq g^l = y

Principe :
-calculer m = partie entière supérieure (sqrt(n))
-creer un dictionnaire qui contient (g^i : i) pour 0<=i<m
-initialiser x = y et  j = 0
-boucle : tant que x n'es pas un clef de D : 
    x = x * g^(-m)
    j = j +1
-retourner jm + D[x] (D est le dictionnaire)


cf explication d'etienne lemonier sur youtube

"""
import math

"""
renvoie (pgcd(g,n), u, v) tq gu + vn = r (u est l'inverse de g)
calcul_inverse_euclide_etendu(120, 23) -> (1, -9, 47) 
ici 47 est l'inverse de 23 mod 120 pour la multi
ici -9 est l'inverse de 120 pour la mul modulo 23
"""
def calcul_inverse_euclide_etendu(g,n):
    if (n == 0):
        return (g,1, 0)
    else:
        (dbis, ubis, vbis) = calcul_inverse_euclide_etendu(n, g%n)
        return (dbis, vbis, ubis - (g//n) * vbis)
    
print(calcul_inverse_euclide_etendu(120, 23))

def modulo_multiplicatif(a,b):
    if(a == 0):
        return b-1
    else:
        var_a = a
        while var_a >= b:
            var_a -= (b)
        while var_a <=0 : 
            var_a += b
        return var_a
    

        

"""
ce code ci ne fonctionne pas du tout, c'est du grand n'importe quoi
def log_pas_de_bebe_pas_de_geants(y,g,n) : 
    m = math.ceil(math.sqrt(n)) 
    print(m)
    D = {}
    puiss_de_g = g
    #pas de bébés
    for i in range (1,m + 1) : 
        D[puiss_de_g] = i
        puiss_de_g = modulo_multiplicatif((puiss_de_g *g),n)
        #puiss_de_g = (puiss_de_g*g)%n
    print(D)
    x = y
    j = 0
    print(x)
    #pas de géants
    cpt = 0
    while x not in D.keys() :
        # ici g^(-m) est (l'inverse de g)^m dans Z/nZ,*
        #x = (x * g^m)%n
        x = modulo_multiplicatif((x*(g**m)),n)
        j = j+1
        print("x = " + str(x))
        cpt += 1
    res = D[x]

    
    NB : on est ici obligé de faire un truc itératif, car dans l'exemple ou 
    on est à un étape égal à 0, si c'est la dernière (ie on fait le modulo direct)
    alors ça va, sinon le résultat récupère un +1 en trop
    (cf exemple pour D[x] = 6, m = 6, cpt = 3)
    
    for i in range (cpt):
        res = modulo_multiplicatif(res - m,n)

    
    return res

"""

def log_pas_de_bebe_pas_de_geants(y,g,n):
    m = int(math.ceil(math.sqrt(n)) )
    #print(m)
    D = {}
    puiss_de_g = g
    #pas de bébés
    for i in range (1,m + 1) : 
        D[puiss_de_g] = i
        puiss_de_g = (puiss_de_g *g)%n
        #puiss_de_g = (puiss_de_g*g)%n
    #print(D)
    x = y
    j = -1
    #print(x)
    #pas de géants
    cpt = 0
    inv_g = calcul_inverse_euclide_etendu(g,n)[1]
    valeur = 1
    inv_g_m = inv_g ** m
    #print("calcul_inverse_passé")
    while x not in D.keys() :

        j = j+1
        # ici g^(-m) est (l'inverse de g)^m dans Z/nZ,*
        x = (y * valeur)%n
        valeur = valeur * inv_g_m
        #x = modulo_multiplicatif((x*(inv_g**m)),n)
        
        #print(x)
        cpt += 1
    #print("j = " + str(j))
    return (D[x] + j*m)%n

#print("modulo_mul")
#print(modulo_multiplicatif((6 - 2*6 ),43))



#print("blabla")
#print("res = " + str(log_pas_de_bebe_pas_de_geants(18,5,97)))
#print("res = " + str(log_pas_de_bebe_pas_de_geants(7,5,23)))

#print((5**19)%23)


import timeit

"""

test = timeit.Timer("log_pas_de_bebe_pas_de_geants(18,5,97)", "from __main__ import log_pas_de_bebe_pas_de_geants")

print(test.timeit(10))

"""

import timeit

def lit_et_resout_pas_de_bebe_pas_de_geant(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    
    i = 0
    tab = ["","","",""]
    for ligne in f:
       
        
        if( i != 4 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "log_pas_de_bebe_pas_de_geants(" + tab[1] + "," +  tab[0] + "," + tab[2] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import log_pas_de_bebe_pas_de_geants" )
            valeur = test.timeit(1)
            dico_temps[int((tab[2])[:-1])] = valeur
            targ = open(target, "a")
            targ.write((tab[2])[:-1] + "\n")
            targ.write(str(valeur) + "\n")
            targ.close()
            i = 0
        #print("un de plus")
        
    f.close()
    
    return dico_temps

print(lit_et_resout_pas_de_bebe_pas_de_geant("fichier_pb_Z_nZ_grand_bis.txt", "temps_pas_de_bebe_pas_de_geant_grand_bis.txt"))
