"""
Note importante : 



calcul du logarithme discret sur un groupe multiplicatif 
type Z/nZ

NB : on suppose g un générateur, donc il ya forcément une solution

entrées : idem que brute force : 
-elément y de G
-g un générateur de G
-n l'ordre de G

Sortie : l de Z/nZ tq g^l = y

Principe général:
-calculer la décomposition en produit de facteurs premiers de n (produit des pi^ei)
-initialiser l à 0 et m à 1
-pour tout (pi,ei) de la décomp de n : 
    -calculer li = récursivité sur la version puissance  de PH (y,g,pi, ei )
    -si m != 1 : calculer l = récursivité sur la version coprime de PH(y,g,s = pi^ei, t= m)
    -sinon mettre l = li
    m devient m * pi^ei
-retourner l

version puissance de PH : 
-g_1 = g^p^(e-1)
- l = 0
-pour i de 1 à e : 
    -calculer z = (y/g^l)^(p^(e-i))
    -calculer le log l_i-1 de z en base g_1 (avec un autre algo genre brute force ou BSGS)
    -mettre à jour l = l + l_i-1*p^i-1
-retourner l

version premier entre eux de pohlig hellman (n = st premiers entre eux)
-calculer y^s y^t g^s g^t
-calculer le log en base l_t = g^t de y^t
-calculer le log en base l_s = g^s de y^s
-calcul des coefs de bezout a,b tq as + bt = 1
-reconstitution par reste chinois : l = asl_t + btl_s
-retourner l mod n


cf explication d'etienne lemonier sur youtube

"""

#algo pour faire les résultion quand n est premier
def casse_log_discret( y,g,n) :
    if(y == 1):
        return 0
    
    val = g
    puiss = 1
    for i in range (1,n) : 
        if val == y : 
            return puiss
        else :
            val = (val * g) % n
            puiss += 1
    return "cette équation n'a pas de solution"



import math

#(cf wikipédia pour la cpx)
def bezout(a,b):
    q = 0
    (r,u,v,r_,u_,v_) = (a,1,0,b,0,1)
    while(r_ != 0) : 
        q = r%r_
        (r,u,v,r_,u_,v_) = (r_, u_, v_, r- (q*r_), u-(q*u_), v - (q*v_))
    return(r,u,v)



def algo_premier_entre_eux(y,g,s,t):
    n = s*t
    y_s = y**s%n
    y_t = y**t%n
    g_s = g**s%n
    g_t = g**t%n
    l_s = casse_log_discret(y_t, g_t, n)
    l_t = casse_log_discret(y_s, g_s, n)
    (z,a,b) = bezout(s,t,1)
    l = a * s * l_t + b * t * l_s
    l = l % (s*t)
    return l


def algo_puissance(y,g,p,e):
    g_1 = g**(p**(e-1))
    l = 0
    for i in range(1, e+1):
        z = (y/(g**l))**(p**(e-i))
        l_i = casse_log_discret(z,g_1, p**e)
        if(l_i == "cette équation n'a pas de solution"):
            return "cette équation n'a pas de solution"
        l = l + l_i * p ** (i-1)
        print("i = "+str(i) + "  y/g^l = " +str(y/(g**l)) + "   l_i = " + str(l_i))
    return l


#possibilité de l'améliorer avec l'olgo rho de pollard
def decomposition_naive(n):
    if(n == 0):
        return [0]
    decomp = []
    d = 2
    cpt = 0
    while (n%d == 0):
        cpt += 1
        q = n//d
        n = q

    if(cpt != 0):
        decomp.append((d,cpt))

    d = 3
    while(d <= n):
        
        cpt = 0
        while (n%d == 0):
            cpt += 1
            q = n//d
            n = q
        #print("d=" + str(d) + "cpt = " + str(cpt))
        if(cpt != 0):
            decomp.append((d,cpt))
        d += 2
    return decomp




def pohlig_hellman(y,g,n):
    decomp = decomposition_naive(n)
    print(decomp)
    l = 0
    m = 1
    for (p,e) in decomp :
        l_i = algo_puissance(y,g,p,e)
        if (m != 1):
            l = algo_premier_entre_eux(y,g,p**e, m)
        else:
            l = l_i
        m = m * p**e
    return l


print(pohlig_hellman(101, 3, 257))

print(pohlig_hellman(18,5,97))

            


