#état : en cours
#source : https://www.youtube.com/watch?v=QjDC1YtQFj4&ab_channel=TommyOcchipinti

"""
renvoie (pgcd(g,n), u, v) tq gu + vn = r (u est l'inverse de g)
calcul_inverse_euclide_etendu(120, 23) -> (1, -9, 47) 
ici 47 est l'inverse de 23 mod 120 pour la multi
ici -9 est l'inverse de 120 pour la mul modulo 23
"""
def calcul_inverse_euclide_etendu(g,n):
    if (n == 0):
        return (g,1, 0)
    else:
        (dbis, ubis, vbis) = calcul_inverse_euclide_etendu(n, g%n)
        return (dbis, vbis, ubis - (g//n) * vbis)

def inverse(g,n):
    return calcul_inverse_euclide_etendu(g,n)[1]






"""
def new_xab( x,a,b, n, grand_n, g, y,):
    if( x%3 == 0 ):
        x = x*x%grand_n
        a = a*2 %n
        b = b*2%n
"""

"""
#varibles de test données par l'algo de base
n = 1018
grand_n = n + 1  # N = 1019 -- prime
g = 2  # generator
y = 5   # 2^{10} = 1024 = 5 (N)
"""


n = 22
grand_n = n+1
g = 5
y = 7


"""
n = 96
grand_n = 97
g = 5
y = 18

"""




"""
n = 17
grand_n = n + 1
g = 3
y = 12
"""


def new_xab(x, a, b,y,g):
    switch_val = x % 3
    if switch_val == 0: # cas 2 pour math 361
        x = (x * x) % grand_n
        a = (a * 2) % n
        b = (b * 2) % n
    elif switch_val == 1: # cas 0 ou 1 pour math 361
        x = (x * g) % grand_n
        a = (a + 1) % n
    else: # cas 0 ou 1 pour math 361
        x = (x * y) % grand_n
        b = (b + 1) % n

    return x, a, b

def rho_pollard(y,g,grand_n):
    n = grand_n - 1

    #x, a, b = 1, 0, 0
    x,a,b = 1, 2, 4
    X, A, B = x, a, b

    for i in range(1, n*n):
        x, a, b = new_xab(x, a, b,y,g) #the turtle
        X, A, B = new_xab(*new_xab(X, A, B,y,g),y,g) # the rabbit
        print(f"{i:3}  {x:4} {a:3} {b:3}  {X:4} {A:3} {B:3}")
        if (x == X and (a!= A or b != B)): # if turtle = rabbit
            break;
        if(i == (n*n)):
            return -10
        
    print((B - b)%n)
    print(a - A)
    #quand on a trouvé tortue = rabbit
    print("resultat :   " + str(((inverse((B-b)%n, grand_n) * (a - A))) %grand_n))
    return (inverse((B-b)%n, grand_n) * (a - A) )%grand_n
    

print(rho_pollard(5,2,1018))


#print("resultat :  " + str(inverse(7, (n-1)) * 1 %(n-1)))

#print((5**78)%97)


import timeit

def lit_et_resout_rho_pollard(fichier : str, target : str):
    dico_temps = {}

    f = open(fichier, "r")
    targ = open(target, "a")
    i = 0
    tab = ["","","",""]
    for ligne in f:
        if( i != 4 ):
            #print(ligne)
            tab[i] = ligne
            i += 1
        else:

            nom_fct = "rho_pollard(" + tab[1] + "," +  tab[0] + "," + tab[2] + ")"
            test = timeit.Timer(nom_fct, "from __main__ import rho_pollard" )
            valeur = test.timeit(1)
            if(eval("rho_pollard(" + tab[1] + "," +  tab[0] + "," + tab[2] + ")") == -10):
                dico_temps[int((tab[2])[:-1])] = 100
                targ.write((tab[2])[:-1] + "\n")
                targ.write(str(valeur) + "\n")
            else : 
                dico_temps[int((tab[2])[:-1])] = valeur
                targ.write((tab[2])[:-1] + "\n")
                targ.write(str(valeur) + "\n")
            i = 0
    f.close()
    targ.close()
    return dico_temps


lit_et_resout_rho_pollard("fichier_pb_Z_nZ_grand_bis.txt", "temps_rho_pollard_grand_bis.txt")

