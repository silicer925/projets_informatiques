

def est_generateur(g_potentiel : int, n : int):
    puiss_g = g_potentiel
    #print(puiss_g)
    for i in range(1, n-2):
        
        puiss_g = (puiss_g * g_potentiel)%n
        #print(puiss_g)
        if(puiss_g == 1):
            return False
    #print("fin boucle")

    puiss_g = (puiss_g * g_potentiel)%n
    #print(puiss_g)
    return puiss_g ==1

#print(est_generateur(5, 97))


def generateur(n : int) : 
    gens = []
    for i in range(2, n):
        if(est_generateur(i,n)):
            gens.append(i)
    if(gens != []):
        return n

#print(generateur(8))



def recherche_generateur(debut : int, fin : int, fichier : str):
    tab_generateur = []
    
    for i in range(debut, fin+1):
        f = generateur(i)
        if(f != None):
            fic = open(fichier, "a")
            fic.write(str(f) + "\n");
            tab_generateur.append(f)
            fic.close()

        #si on veut faire apparaitre les None
        #tab_generateur.append(generateur(i))
    
    return "okey"

#print(recherche_generateur(8001,9000, "premiers.txt"))

def trouve_des_nb(a,b):
    alpha = 1
    beta = 1
    while((beta * 23)% 31 != alpha):
        alpha +=1
        for i in range(1, beta):
            if ((beta * 23)% 31 == i):
                return (i,beta)
    return(alpha, beta)


print(trouve_des_nb(23, 31))

print(2**20248)
print(len(str(2**2048)))
print(2**200)
print(len(str(2**200)))