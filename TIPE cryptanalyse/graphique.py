import matplotlib.pyplot as plt


def cree_graphique(tab_fichier,noms):
    cpt = 0
    for elt in tab_fichier:
        tab_abs = []
        tab_ord = []
        f = open(elt, "r")
        i = 0
        for ligne in f:

            #print(i)
            #print(ligne)
            if(i%2 == 0):#si on est sur une abscisse
                tab_abs.append(int( ligne[:-1]))
                
            else:
                tab_ord.append(float(ligne[:-1]))
            i+=1   
        #print(tab_abs)
        #print(tab_ord)
        plt.plot(tab_abs, tab_ord, "--", label = noms[cpt])
        cpt+=1
    plt.xlabel("p premier")
    plt.ylabel("temps d'execution")
    plt.legend()
    plt.show()


def cree_graphique_seuil(tab_fichier,noms):
    cpt = 0
    for elt in tab_fichier:
        tab_abs = []
        tab_ord = []
        f = open(elt, "r")
        i = 0
        for ligne in f:

            #print(i)
            #print(ligne)
            if(i%2 == 0):#si on est sur une abscisse
                valeur = int( ligne[:-1])
            else:
                if(float(ligne[:-1]) <= 0.02):
                    tab_ord.append(float(ligne[:-1]))
                    tab_abs.append(valeur)
            i+=1   
        #print(tab_abs)
        #print(tab_ord)
        plt.plot(tab_abs, tab_ord, "--", label = noms[cpt])
        cpt+=1
    plt.xlabel("p premier")
    plt.ylabel("temps d'execution")
    plt.legend()
    plt.show()

#cree_graphique(["temps_brute_force.txt", "brute force"])
#cree_graphique(["temps_pas_de_bebe_pas_de_geant.txt", "pbpg"])
#cree_graphique_seuil([  "temps_brute_force_grand_bis.txt"], ["brute force"])

tab_abs = [999809,9000011,11000027,21000037,31000003,41000017,51000041,61000001,71000051,81000001,91000009,99000007]
tab_ord = [1, 10, 100, 500, 500, 1 ,10, 100, 500, 1, 10, 500]

plt.plot(tab_abs,tab_ord)
plt.xlabel("p premier")
plt.ylabel("temps d'éxécution en ms")
plt.legend()
plt.show()





