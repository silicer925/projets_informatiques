#include <iostream>
#include <time.h>//pour random
#include <fstream>//pour �crire sur un doc texte
#include <string>//pour �crire sur un doc texte
#include <windows.h>
#include <conio.h>
#include <thread>
#include <stdio.h>
#include "game.hpp"

using namespace std;

/*
sur un ou (||) le vrai l'emporte sur le faux inverse du et
! négation sur une condition
but du while tant que c'est vrai alors on continu la boucle
*/
void numero(unsigned short numero){
    switch(numero){
        case 1 : PlaySound("musique/1.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 2 : PlaySound("musique/2.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 3 : PlaySound("musique/3.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                ;break;
        case 4 : PlaySound("musique/4.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 5 : PlaySound("musique/5.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 6 : PlaySound("musique/6.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 7 : PlaySound("musique/7.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 8 : PlaySound("musique/8.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        case 9 : PlaySound("musique/9.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                break;
        default : cout<<"Probl�me au niveau des nombres de la musique"<<endl;
    }
}

void nettoyer(){
    for(unsigned short i;i<40;i++)cout<<endl;
    //system("cls");//clear la console
}

void temp(unsigned short s){
    cout<<endl<<"attente de "<<s<<" s"<<endl;
    for(unsigned short i=1;i<=s;i++){
        Sleep(1000);
        cout<<'#';
    }
    cout<<endl;
}

void game::test_fin(){
    unsigned short mechant=0;
    for(unsigned short h=1;h<longueur;h++){
        if(compo[h].id==2 || compo[h].id==4)mechant=1;
    }
    if(mechant==0)cout<<"Victoire du village"<<endl;
    unsigned short carte_en_vie=0,i=0;
    while(i<longueur || carte_en_vie<2){
        if(compo[i].vie!=0)carte_en_vie++;
        i++;
    }
    if(carte_en_vie==1){
        unsigned short i=1,le_dernier=0;
        while(compo[i].vie==0){
            i++;
            le_dernier=compo[i].id;
        }
        cout<<"Victoire du dernier restant : "<<le_dernier<<"numero : "<<i<<endl;
    }
    if(carte_en_vie==0)cout<<"Victoire des villageois"<<endl;
}

game::game(unsigned short nombre_carte){
    longueur=nombre_carte;//mise de la longeur en param�tre
    string mon_fichier = "log/constructeur.txt";
    ofstream fichier(mon_fichier.c_str(), ios::out | ios::trunc);// on ouvre le fichier en lecture qui est le chemin en fonction de l'executable
 	//le ios::out estpour l'�criture et le ios::trunc est pour la suppression de son contenu

    if(fichier){
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"-------------------------Debut du tirage----------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        compo=new carte[longueur];
        fichier<<"Debut de l'initialisation des variables"<<endl;
        for(unsigned char i=0;i<longueur;i++){//tableau de 1 a 25 le 0 = on s'en fous
            compo[i].derniere_action=0;
            compo[i].vie=1;
            compo[i].id=1;
            compo[i+1].joueur=((i-(i%5))/5)+1;
            compo[i+1].carte=(i%5)+1;
            compo[i].precedent=0;
        }
        fichier<<"Fin de l'initialisation des variables"<<endl<<endl;
        fichier<<"Demande du nombre de cartes"<<endl;
        cout<<"Lg :";pos.lg=(getch())-48;cout<<" "<<pos.lg<<endl;
        cout<<"Sorciere :";pos.sorciere=(getch())-48;cout<<" "<<pos.sorciere<<endl;
        cout<<"Voyante :";pos.voyante=(getch())-48;cout<<" "<<pos.voyante<<endl;
        cout<<"Assassin :";pos.assassin=(getch())-48;cout<<" "<<pos.assassin<<endl;
        fichier<<"Fin de la demande du nombre de cartes"<<endl<<"nombre de lg(s): "<<pos.lg<<endl<<"nombre de sorciere(s): "<<pos.sorciere<<endl<<"nombre de voyante(s): "<<pos.voyante<<endl<<"nombre d'assassin(s): "<<pos.assassin<<endl<<endl;

        nettoyer();
        fichier<<"clear de la console"<<endl;
        unsigned short var=0,j=0;

        /*void attribution(unsigned short nombre_de_role,unsigned short identifiant){
            unsigned short j=0,var=0;
            for(unsigned char i=0;i<nombre_de_role;i++){//en fonction du nombre de lg
                while(compo[var].id!=1)var=rand()%25+1;
                compo[var].id=identifiant;
                if(nombre_de_role>1)compo[var].numero=j++;
            }
        }*/

        for(unsigned char i=0;i<pos.lg;i++){//en fonction du nombre de lg
            do{
               var=rand()%25+1;
            }while(compo[var].id!=1);
            compo[var].id=2;
            if(pos.lg>1){
                j++;
                compo[var].numero=j;
            }
        }
        j=0;
        fichier<<"Mise du nombre de lg(s)"<<endl;
        for(unsigned char i=0;i<pos.sorciere;i++){//en fonction du nombre de sorcieres
            do{
               var=rand()%25+1;
            }while(compo[var].id!=1);
            compo[var].id=3;
            if(pos.sorciere>1){
                j++;
                compo[var].numero=j;
            }
        }
        j=0;
        fichier<<"Mise du nombre de sorciere(s)"<<endl;
        for(unsigned char i=0;i<pos.voyante;i++){//en fonction du nombre de voyante
            do{
               var=rand()%25+1;
            }while(compo[var].id!=1);
            compo[var].id=4;
            if(pos.voyante>1){
                j++;
                compo[var].numero=j;
            }
        }
        j=0;
        fichier<<"Mise du nombre de voyante(s)"<<endl;
        for(unsigned char i=0;i<pos.assassin;i++){//en fonction du nombre d'assassin
            do{
               var=rand()%25+1;
            }while(compo[var].id!=1);
            compo[var].id=5;
            if(pos.assassin>1){
                j++;
                compo[var].numero=j;
            }
        }
        j=0;
        fichier<<"Mise du nombre d'assassin(s)"<<endl;

        fichier<<"Fin de l'attribution des roles"<<endl;
        for(unsigned short i=1;i<longueur;i++){//lire les roles ajout�s
            fichier<<"Carte "<<i<<": id="<<compo[i].id<<" numero="<<compo[i].numero<<" joueur="<<compo[i].joueur<<" carte="<<compo[i].carte<<" vie="<<compo[i].vie<<" precedent="<<compo[i].precedent<<endl;
        }
        fichier<<"Debut de l'affichage pour les joueurs des roles"<<endl;
        PlaySound("musique/fred.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        affiche(compo[1].id);affiche(compo[2].id);affiche(compo[3].id);affiche(compo[4].id);affiche(compo[5].id);
        temp(8);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        for(unsigned short k=0;k<50;k++)cout<<endl;
        temp(2);
        PlaySound("musique/maxi.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        affiche(compo[6].id);affiche(compo[7].id);affiche(compo[8].id);affiche(compo[9].id);affiche(compo[10].id);
        temp(8);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        for(unsigned short k=0;k<50;k++)cout<<endl;
        temp(2);
        PlaySound("musique/gut.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        affiche(compo[11].id);affiche(compo[12].id);affiche(compo[13].id);affiche(compo[14].id);affiche(compo[15].id);
        temp(8);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        for(unsigned short k=0;k<50;k++)cout<<endl;
        temp(2);
        PlaySound("musique/nit.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        affiche(compo[16].id);affiche(compo[17].id);affiche(compo[18].id);affiche(compo[19].id);affiche(compo[20].id);
        temp(8);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        for(unsigned short k=0;k<50;k++)cout<<endl;
        temp(2);
        PlaySound("musique/val.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        affiche(compo[21].id);affiche(compo[22].id);affiche(compo[23].id);affiche(compo[24].id);affiche(compo[25].id);
        temp(8);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        for(unsigned short k=0;k<50;k++)cout<<endl;
        temp(2);
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------Fin du tirage-----------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier.close();
    }else{
        cout<<"Impossible d'ouvrir le fichier !"<<endl;// ouverture en �criture avec effacement du fichier ouvert
    }
}
void game::action(int unsigned short id){
    /*string mon_fichier = "log/action.txt";
    ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);// on ouvre le fichier en lecture qui est le chemin en fonction de l'executable
 	//le ios::out estpour l'�criture et le ios::app est pour aller a la fin du fichier pour �crire
    if(fichier){
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"------------------------- Nuit "<<jour_nuit<<" ----------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        if(pos.lg>1 && id==2){
            PlaySound("musique/tous_les_lgs.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
            unsigned short lg_en_vie=0;
            for(unsigned short i=1;i<longueur;i++){
                if(compo[i].vie!=0 && compo[i].id==2){
                    lg_en_vie++;
                }
                if(lg_en_vie>=2){
                    i=longueur;
                    cout<<"Reunion des lgs :"<<endl<<endl;
                    temp(10);
                }
            }
            PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        }
        unsigned short numero=0;
        for(unsigned short i=1;i<longueur;i++){
            if((compo[i].vie!=0 && compo[i].id==id) || (compo[i].vie==0 && compo[i].id==3)){//si la carte est en vie et que c'est l'id demand�
                numero++;
                unsigned short ok=0;
                switch(id){
                    case 2 :PlaySound("musique/le_lg_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            if(pos.lg>1)ok=1;
                            break;
                    case 3 : PlaySound("musique/sorciere_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            if(pos.sorciere>1)ok=1;
                            break;//id = 3 soso
                    case 4 :PlaySound("musique/voyante_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            if(pos.voyante>1)ok=1;
                            break;//id = 4 voyante
                    case 5 : PlaySound("musique/assassin_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            if(pos.assassin>1)ok=1;
                            break;//id = 5 assassin
                    default : cout<<"Probl�me au niveau des roles"<<endl;
                }
                if(ok==1){
                    switch(numero){
                        case 1 : PlaySound("musique/1.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 2 : PlaySound("musique/2.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 3 : PlaySound("musique/3.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                ;break;
                        case 4 : PlaySound("musique/4.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 5 : PlaySound("musique/5.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 6 : PlaySound("musique/6.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 7 : PlaySound("musique/7.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 8 : PlaySound("musique/8.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        case 9 : PlaySound("musique/9.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                                break;
                        default : cout<<"Probl�me au niveau des nombres de la musique"<<endl;
                    }
                }
                unsigned short choix=0;
                unsigned short action=0; // 1 pour tuer // 2 pour voir //3 pour res
                switch(id){
                    case 2 : cout<<"Tour d'un lg : "<<numero<<endl;
                            cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                            cout<<"Que voulez vous faire :"<<endl<<" - 1 : Tuer"<<endl<<" - 2 : Ne rien faire"<<endl;
                            break;//id = 2=lg
                    case 3 : cout<<"Tour de la sorci�re : "<<numero<<endl;
                            cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                            cout<<"Que voulez vous faire :"<<endl;
                            if(compo[i].derniere_action!=1)cout<<" - 1 : Tuer"<<endl;
                            cout<<" - 2 : Ne rien faire"<<endl;
                            if(compo[i].derniere_action!=3)cout<<" - 3 : Ressuciter"<<endl;
                            if(compo[i].vie==0)cout<<endl<<"Attention vous devez vous ressuciter car vous avez ete tuee !!!"<<endl<<endl;
                            break;//id = 3 soso
                    case 4 : cout<<"Tour de la voyante : "<<numero<<endl;
                            cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                            cout<<"Que voulez vous faire :"<<endl<<" - 1 : Regarder une carte"<<endl<<" - 2 : Ne rien faire"<<endl;
                            ;break;//id = 4 voyante
                    case 5 : cout<<"Tour de l'assassin : "<<numero<<endl;
                            cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                            cout<<"Que voulez vous faire :"<<endl<<" - 1 : Tuer"<<endl<<" - 2 : Ne rien faire"<<endl;
                            break;//id = 5 assassin
                    default : cout<<"Probleme au niveau des roles"<<endl;
                }
                if(id==2 && pos.lg>1){//pour afficher les mates loups
                    unsigned short memoire_joueur=0;
                    for(unsigned short g=1;g<=longueur;g++){
                        if(compo[g].vie!=0 && compo[g].id==2 && g!=i && memoire_joueur!=compo[g].joueur && compo[i].joueur!=compo[g].joueur){//si en vie si c'est un lg si ce n'est pas la carte que l'on est si ce n'est pas deux fois la meme personne si ce n'est pas le joueur qui joue ne mate
                            memoire_joueur=compo[g].joueur;
                            if(g==25)memoire_joueur=5;
                            cout<<"Mate(s) loup est le joueur "<<memoire_joueur<<endl;
                        }
                    }
                }
                unsigned j=0,temps=0;
                unsigned short max_tempo=1000;
                while(temps<=max_tempo){
                    Sleep(1);
                    if(!kbhit()){
                        temps++;
                        if(temps==j+100){
                            j=temps;
                            cout<<'#';
                        }
                    }else{
                        choix=(getch())-48;
                        cout<<"La valeur est : "<<choix<<endl;
                        temps=max_tempo;
                        switch(id){
                            case 2 :fichier<<"Tour d'un lg : "<<numero<<endl;
                                    if(choix==1)action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 2=lg
                            case 3 : fichier<<"Tour de la sorci�re : "<<numero<<endl;
                                    if((choix==1)||(choix=3))action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 3 soso
                            case 4 : fichier<<"Tour de la voyante : "<<numero<<endl;
                                    if(choix==1)action=2;
                                    if(choix==2)action=0;
                                    break;//id = 4 voyante
                            case 5 : fichier<<"Tour de l'assassin : "<<numero<<endl;
                                    if(choix==1)action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 5 assassin
                            default : cout<<"Probl�me au niveau des roles"<<endl;
                        }

                        if(action==1)fichier<<"Le choix est de tuer"<<endl;
                        if(action==2)fichier<<"Le choix est de voir"<<endl;
                        if(action==3)fichier<<"Le choix est de ressuciter"<<endl;
                        if(action<1 || action>3)fichier<<"Le choix est de ne rien faire"<<endl;

                        if((action==1)||(action==2)){
                            unsigned short temp;
                            unsigned short erreur=0;
                            do{
                                erreur=0;
                                if(compo[1].vie!=0 || compo[2].vie!=0 || compo[3].vie!=0 || compo[4].vie!=0 || compo[5].vie!=0)cout<<"Joueur 1"<<endl;
                                if(compo[6].vie!=0 || compo[7].vie!=0 || compo[8].vie!=0 || compo[9].vie!=0 || compo[10].vie!=0)cout<<"Joueur 2"<<endl;
                                if(compo[11].vie!=0 || compo[12].vie!=0 || compo[13].vie!=0 || compo[14].vie!=0 || compo[15].vie!=0)cout<<"Joueur 3"<<endl;
                                if(compo[16].vie!=0 || compo[17].vie!=0 || compo[18].vie!=0 || compo[19].vie!=0 || compo[20].vie!=0)cout<<"Joueur 4"<<endl;
                                if(compo[21].vie!=0 || compo[22].vie!=0 || compo[13].vie!=0 || compo[24].vie!=0 || compo[25].vie!=0)cout<<"Joueur 5"<<endl;
                                temp=(getch())-48;choix=(temp*5)-5;cout<<" "<<temp<<endl;
                                if(compo[choix+1].vie!=0)cout<<"Carte 1"<<endl;
                                if(compo[choix+2].vie!=0)cout<<"Carte 2"<<endl;
                                if(compo[choix+3].vie!=0)cout<<"Carte 3"<<endl;
                                if(compo[choix+4].vie!=0)cout<<"Carte 4"<<endl;
                                if(compo[choix+5].vie!=0)cout<<"Carte 5"<<endl;
                                temp=(getch())-48;choix=choix+temp;cout<<" "<<temp<<endl;

                                if(compo[choix].vie!=0 && choix!=i && choix<=25 && choix>=1){//si en vie et que ce n'est pas sois meme qe l'on vise et que la carte existe
                                    cout<<"la carte selectionne est : "<<choix<<endl;
                                    fichier<<"la carte selectionne est : "<<choix<<endl;
                                    if(compo[choix].id!=5 && action==1){//si ce n'est pas l'assassin
                                        compo[choix].vie--;//on tue la carte cibl�e
                                        compo[choix].precedent=1;//on dis que la carte � �t� tu�
                                    }
                                    if(action==2){
                                        cout<<"La carte: "<<endl;
                                        fichier<<"affiche le role"<<endl;
                                        affiche_simple(compo[choix].id);
                                        cout<<endl;
                                    }
                                }else{erreur=1;};
                            }while(erreur!=0);
                            compo[i].derniere_action=1;
                        }
                        if(action==3){//pour ressuciter
                            unsigned short nombre=1;
                            unsigned short place[5];
                            for(unsigned short j=0;j<5;j++)place[j]=0;
                            for(unsigned short j=1;j<longueur;j++){
                                if(compo[j].precedent==1){
                                    cout<<"Carte "<<nombre<<"  ";
                                    affiche_simple(compo[j].id);
                                    place[nombre]=j;
                                    nombre++;
                                }
                            }
                            nombre=0;
                            nombre=(getch())-48;
                            cout<<nombre<<endl;
                            compo[place[nombre]].vie++;//on remet une carte une vie
                            compo[place[nombre]].precedent=0;//on ma remet come en vie
                            compo[i].derniere_action=3;
                        }
                    }
                }
                temp(2);
                PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                nettoyer();
            }
        }
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"---------------------- Fin de la nuit "<<jour_nuit<<" -------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl<<endl;//80 tirets
        fichier.close();
    }else{
        cout<<"Impossible d'ouvrir le fichier !"<<endl;// ouverture en �criture avec effacement du fichier ouvert
    }*/
    string mon_fichier = "log/action.txt";
    ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);// on ouvre le fichier en lecture qui est le chemin en fonction de l'executable
 	//le ios::out estpour l'�criture et le ios::app est pour aller a la fin du fichier pour �crire
    if(fichier){
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"------------------------- Nuit "<<jour_nuit<<" ----------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        unsigned short numero=1;
        for(unsigned short i=1;i<longueur;i++){
            if((compo[i].vie!=0 && compo[i].id==id) || (compo[i].vie==0 && compo[i].id==3)){//si la carte est en vie et que c'est l'id demand�
                switch(id){
                    case 2 :
                        if(pos.lg>1){
                            PlaySound("musique/tous_les_lgs.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            unsigned short lg_en_vie=0;
                            for(unsigned short i=1;i<longueur;i++){
                                if(compo[i].vie!=0 && compo[i].id==2){
                                    lg_en_vie++;
                                }
                                if(lg_en_vie>=2){
                                    i=longueur;
                                    cout<<"Reunion des lgs :"<<endl<<endl;
                                    temp(10);
                                }
                            }
                            PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                            //pour afficher les mates loups
                            unsigned short memoire_joueur=0;
                            for(unsigned short g=1;g<=longueur;g++){
                                if(compo[g].vie!=0 && compo[g].id==2 && g!=i && memoire_joueur!=compo[g].joueur && compo[i].joueur!=compo[g].joueur){//si en vie si c'est un lg si ce n'est pas la carte que l'on est si ce n'est pas deux fois la meme personne si ce n'est pas le joueur qui joue ne mate
                                    memoire_joueur=compo[g].joueur;
                                    if(g==25)memoire_joueur=5;
                                    cout<<"Mate(s) loup est le joueur "<<memoire_joueur<<endl;
                                }
                            }
                        }
                        PlaySound("musique/le_lg_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                        cout<<"Tour d'un lg : "<<numero<<endl;
                        fichier<<"Tour d'un lg : "<<numero<<endl;
                        cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                        cout<<"Que voulez vous faire :"<<endl<<" - 1 : Tuer"<<endl<<" - 2 : Ne rien faire"<<endl;
                        break;

                    case 3 :
                        PlaySound("musique/sorciere_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                        cout<<"Tour de la sorciere : "<<numero<<endl;
                        fichier<<"Tour de la sorciere : "<<numero<<endl;
                        cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                        cout<<"Que voulez vous faire :"<<endl;
                        if(compo[i].derniere_action!=1)cout<<" - 1 : Tuer"<<endl;
                        cout<<" - 2 : Ne rien faire"<<endl;
                        if(compo[i].derniere_action!=3)cout<<" - 3 : Ressuciter"<<endl;
                        if(compo[i].vie==0)cout<<endl<<"Attention vous devez vous ressuciter car vous avez ete tuee !!!"<<endl<<endl;
                        break;
                    case 4 :
                        PlaySound("musique/voyante_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                        cout<<"Tour de la voyante : "<<numero<<endl;
                        fichier<<"Tour de la voyante : "<<numero<<endl;
                        cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                        cout<<"Que voulez vous faire :"<<endl<<" - 1 : Regarder une carte"<<endl<<" - 2 : Ne rien faire"<<endl;
                        break;
                    case 5 :
                        PlaySound("musique/assassin_reveil.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
                        cout<<"Tour de l'assassin : "<<numero<<endl;
                        fichier<<"Tour de l'assassin : "<<numero<<endl;
                        cout<<"Vous etes la carte n "<<i<<" joueur "<<compo[i].joueur<<" carte n "<<compo[i].carte<<endl;
                        cout<<"Que voulez vous faire :"<<endl<<" - 1 : Tuer"<<endl<<" - 2 : Ne rien faire"<<endl;
                        break;
                    default: cout<<"erreur"<<endl;
                }
                unsigned short choix=0;
                unsigned short action=0; // 1 pour tuer // 2 pour voir //3 pour res
                unsigned j=0,temps=0;
                unsigned short max_tempo=1000;
                while(temps<=max_tempo){
                    Sleep(1);
                    if(!kbhit()){
                        temps++;
                        if(temps==j+100){
                            j=temps;
                            cout<<'#';
                        }
                    }else{
                        choix=(getch())-48;
                        cout<<"La valeur est : "<<choix<<endl;
                        temps=max_tempo;
                        switch(id){
                            case 2 :if(choix==1)action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 2=lg
                            case 3 :if((choix==1)||(choix=3))action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 3 soso
                            case 4 :if(choix==1)action=2;
                                    if(choix==2)action=0;
                                    break;//id = 4 voyante
                            case 5 :if(choix==1)action=choix;
                                    if(choix==2)action=0;
                                    break;//id = 5 assassin
                            default : cout<<"Probl�me au niveau des roles"<<endl;
                        }

                        if(action==1)fichier<<"Le choix est de tuer"<<endl;
                        if(action==2)fichier<<"Le choix est de voir"<<endl;
                        if(action==3)fichier<<"Le choix est de ressuciter"<<endl;
                        if(action<1 || action>3)fichier<<"Le choix est de ne rien faire"<<endl;

                        if((action==1)||(action==2)){
                            unsigned short temp;
                            unsigned short erreur=0;
                            do{
                                erreur=0;
                                if(compo[1].vie!=0 || compo[2].vie!=0 || compo[3].vie!=0 || compo[4].vie!=0 || compo[5].vie!=0)cout<<"Joueur 1"<<endl;
                                if(compo[6].vie!=0 || compo[7].vie!=0 || compo[8].vie!=0 || compo[9].vie!=0 || compo[10].vie!=0)cout<<"Joueur 2"<<endl;
                                if(compo[11].vie!=0 || compo[12].vie!=0 || compo[13].vie!=0 || compo[14].vie!=0 || compo[15].vie!=0)cout<<"Joueur 3"<<endl;
                                if(compo[16].vie!=0 || compo[17].vie!=0 || compo[18].vie!=0 || compo[19].vie!=0 || compo[20].vie!=0)cout<<"Joueur 4"<<endl;
                                if(compo[21].vie!=0 || compo[22].vie!=0 || compo[13].vie!=0 || compo[24].vie!=0 || compo[25].vie!=0)cout<<"Joueur 5"<<endl;
                                temp=(getch())-48;choix=(temp*5)-5;cout<<" "<<temp<<endl;
                                if(compo[choix+1].vie!=0)cout<<"Carte 1"<<endl;
                                if(compo[choix+2].vie!=0)cout<<"Carte 2"<<endl;
                                if(compo[choix+3].vie!=0)cout<<"Carte 3"<<endl;
                                if(compo[choix+4].vie!=0)cout<<"Carte 4"<<endl;
                                if(compo[choix+5].vie!=0)cout<<"Carte 5"<<endl;
                                temp=(getch())-48;choix=choix+temp;cout<<" "<<temp<<endl;

                                if(compo[choix].vie!=0 && choix!=i && choix<=25 && choix>=1){//si en vie et que ce n'est pas sois meme qe l'on vise et que la carte existe
                                    cout<<"la carte selectionne est : "<<choix<<endl;
                                    fichier<<"la carte selectionne est : "<<choix<<endl;
                                    if(compo[choix].id!=5 && action==1){//si ce n'est pas l'assassin
                                        compo[choix].vie--;//on tue la carte cibl�e
                                        compo[choix].precedent=1;//on dis que la carte � �t� tu�
                                    }
                                    if(action==2){
                                        cout<<"La carte: "<<endl;
                                        fichier<<"affiche le role"<<endl;
                                        affiche_simple(compo[choix].id);
                                        cout<<endl;
                                    }
                                }else{erreur=1;};
                            }while(erreur!=0);
                            compo[i].derniere_action=1;
                        }
                        if(action==3){//pour ressuciter
                            unsigned short nombre=1;
                            unsigned short place[5];
                            for(unsigned short j=0;j<5;j++)place[j]=0;
                            for(unsigned short j=1;j<longueur;j++){
                                if(compo[j].precedent==1){
                                    cout<<"Carte "<<nombre<<"  ";
                                    affiche_simple(compo[j].id);
                                    place[nombre]=j;
                                    nombre++;
                                }
                            }
                            nombre=0;
                            nombre=(getch())-48;
                            cout<<nombre<<endl;
                            compo[place[nombre]].vie++;//on remet une carte une vie
                            compo[place[nombre]].precedent=0;//on ma remet come en vie
                            compo[i].derniere_action=3;
                        }
                    }
                }
            }
            numero++;
        }
        temp(2);
        PlaySound("musique/vous_pouvez_vous_rendormir.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termine
        nettoyer();
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"---------------------- Fin de la nuit "<<jour_nuit<<" -------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl<<endl;//80 tirets
        fichier.close();
    }else{
        cout<<"Impossible d'ouvrir le fichier !"<<endl;// ouverture en �criture avec effacement du fichier ouvert
    }
}

void game::jour(){
    string mon_fichier = "log/jour.txt";
    ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);// on ouvre le fichier en lecture qui est le chemin en fonction de l'executable
 	//le ios::out estpour l'�criture et le ios::app est pour aller a la fin du fichier pour �crire
    if(fichier){
        fichier<<endl;
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"------------------------- Jour "<<jour_nuit<<" ----------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        PlaySound("musique/le_village_se_reveille.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termin
        fichier<<"Les morts sont :"<<endl;
        cout<<"Vote le jour"<<endl<<"Les morts sont :"<<endl;
        for(unsigned short i=1;i<longueur;i++){ //permet de verifier les morts
            if(compo[i].precedent==1){//si ils sont mort cette nuit et si ce n'est pas la sorcière
                compo[i].precedent=0;
                affiche_simple(compo[i].id);
                cout<<"Joueur= "<<compo[i].joueur<<" | Carte n = "<<compo[i].carte<<" | id= "<<compo[i].id<<"numero "<<i<<endl<<endl;
                fichier<<"Joueur= "<<compo[i].joueur<<" | Carte n = "<<compo[i].carte<<" | id= "<<compo[i].id<<endl<<endl;
            }
        }
        test_fin();
        cout<<"choix des personnes a tuer :"<<endl;
        fichier<<"choix des personnes a tuer :"<<endl;
        for(unsigned short i=1;i<=2;i++){//car 2 votes
            cout<<"Carte "<<i<<" a tuer par le village"<<endl;
            unsigned short temp=0;
            unsigned short choix=0;
            do{
                if(compo[1].vie!=0 || compo[2].vie!=0 || compo[3].vie!=0 || compo[4].vie!=0 || compo[5].vie!=0)cout<<"Joueur 1"<<endl;
                if(compo[6].vie!=0 || compo[7].vie!=0 || compo[8].vie!=0 || compo[9].vie!=0 || compo[10].vie!=0)cout<<"Joueur 2"<<endl;
                if(compo[11].vie!=0 || compo[12].vie!=0 || compo[13].vie!=0 || compo[14].vie!=0 || compo[15].vie!=0)cout<<"Joueur 3"<<endl;
                if(compo[16].vie!=0 || compo[17].vie!=0 || compo[18].vie!=0 || compo[19].vie!=0 || compo[20].vie!=0)cout<<"Joueur 4"<<endl;
                if(compo[21].vie!=0 || compo[22].vie!=0 || compo[13].vie!=0 || compo[24].vie!=0 || compo[25].vie!=0)cout<<"Joueur 5"<<endl;
                temp=(getch())-48;choix=temp*5;
                if(compo[choix+1].vie!=0)cout<<"Carte 1"<<endl;
                if(compo[choix+2].vie!=0)cout<<"Carte 2"<<endl;
                if(compo[choix+3].vie!=0)cout<<"Carte 3"<<endl;
                if(compo[choix+4].vie!=0)cout<<"Carte 4"<<endl;
                if(compo[choix+5].vie!=0)cout<<"Carte 5"<<endl;
                temp=(getch())-48;choix=choix+temp;
            }while(compo[choix].vie==0 || choix>=25 || choix<=1); //compo[choix].vie==0 && choix>=25 && choix<=1
            cout<<"la carte selectionne est : "<<choix<<endl;
            fichier<<"la carte selectionne est : "<<choix<<endl;
            affiche_simple(compo[choix].id);
            compo[choix].vie=0;//on tue la carte ciblee en enlevant toutes ses vies
        }
        test_fin();
        PlaySound("musique/le_village_sendort.wav", GetModuleHandle(NULL), SND_FILENAME);// cela joue jusque la piste se termin
        nettoyer();
        fichier<<"--------------------------------------------------------------------------------"<<endl;//80 tirets
        fichier<<"---------------------- Fin du jour "<<jour_nuit<<" -------------------------------------"<<endl;//80 tirets
        fichier<<"--------------------------------------------------------------------------------"<<endl<<endl;//80 tirets
        jour_nuit++;
        fichier.close();
    }else{
        cout<<"Impossible d'ouvrir le fichier !"<<endl;// ouverture en �criture avec effacement du fichier ouvert
    }
}

void game::affiche(unsigned short id){
    switch(id){
        case 1 : cout<<"Role : "<<"Villageois"<<endl<<"Doit juste gagner avec le village"<<endl<<endl;break;
        case 2 : cout<<"Role : "<<"Loup Garou"<<endl<<"Doit gagner avec ses confreres loups"<<endl<<endl;break;
        case 3 : cout<<"Role : "<<"Sorciere"<<endl<<"Doit gagner avec le village"<<endl<<endl;break;
        case 4 : cout<<"Role : "<<"Voyante"<<endl<<"Doit gagner avec le village"<<endl<<endl;break;
        case 5 : cout<<"Role : "<<"Assassin"<<endl<<"Doit tuer tous le monde pour etre le dernier survivant"<<endl<<endl;break;
        default : cout<<"Probleme d'affichage des roles "<<id<<endl<<endl;
    }
}
void game::affiche_simple(unsigned short id){
    switch(id){
        case 1 : cout<<"Role : "<<"Villageois"<<endl<<endl;break;
        case 2 : cout<<"Role : "<<"Loup Garou"<<endl<<endl;break;
        case 3 : cout<<"Role : "<<"Sorciere"<<endl<<endl;break;
        case 4 : cout<<"Role : "<<"Voyante"<<endl<<endl;break;
        case 5 : cout<<"Role : "<<"Assassin"<<endl<<endl;break;
        default : cout<<"Probleme d'affichage des roles "<<id<<endl<<endl;
    }
}
/*
    string mon_fichier = "log/test.txt";
    ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);// on ouvre le fichier en lecture qui est le chemin en fonction de l'executable
 	//le ios::out estpour l'�criture et le ios::app est pour aller a la fin du fichier pour �crire
    if(fichier){
        fichier<<endl;
        fichier.close();
    }else{
        cout<<"Impossible d'ouvrir le fichier !"<<endl;// ouverture en �criture avec effacement du fichier ouvert
    }

*/

/*unsigned j=0,i=0;
while(i<=100000){
    Sleep(1);
    if(!kbhit()){
        i++;
        if(i==j+100){
            j=i;
            cout<<"time : "<<j/100<<"s"<<endl;
        }
    }else{
        unsigned short valeur=(getch())-48;
        cout<<"La valeur est : "<<valeur<<endl;
        i=100000;
    }
}*/
