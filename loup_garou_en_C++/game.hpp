#include <iostream>
#include <time.h>//pour random
#include <windows.h>

using namespace std;

/*
Joueur 1 : Fred
Joueur 2 : bayb
Joueur 3 : Gut
Joueur 4 : nit
Joueur 5 : Val
*/

struct carte{
    unsigned short vie;//permet de savoir le nombre de vie restant
    unsigned short numero;//savoir quel role parmis plusieirs
    unsigned short joueur;
    unsigned short carte;
    unsigned short id;//savoir le role
    unsigned short precedent;
    unsigned short derniere_action;
    /*
    id 1 = villageois
    id 2 = lg
    id 3 = soso
    id 4 = voyante
    id 5 = assassin
    */
};

struct role{
    unsigned short lg;
    unsigned short voyante;
    unsigned short sorciere;
    unsigned short assassin;
    /*
    id 1 = villageois
    id 2 = lg
    id 3 = soso
    id 4 = voyante
    id 5 = assassin
    */
};

class game{
    carte *compo;
    role pos;
    unsigned short longueur=25;
    unsigned short jour_nuit=0;
    public:game(unsigned short longeur2);
        void action(unsigned short id);
        void affiche(unsigned short id);
        void affiche_simple(unsigned short id);
        void jour();
        void test_fin();
};
