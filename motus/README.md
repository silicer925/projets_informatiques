## Le trinôme de Newton

# Nom
PICQUET augustine

# Classe 
MP2I

# Année
2023

# Jeu choisi
Motus (sorte de Mastermind avec des mots)

# Commande pour compiler
ocamlc -c utilitaire.mli ; ocamlc -c utilitaire.ml ; ocamlc -c affichage.mli ; ocamlc -c affichage.ml ; ocamlc -c joueur.mli ; ocamlc -c joueur.ml ; ocamlc -c ordi_hasard.mli ; ocamlc -c ordi_hasard.ml ; ocamlc -c ordi_intelligent.mli ; ocamlc -c ordi_intelligent.ml ; ocamlc -c jeu.mli ; ocamlc -c jeu.ml ; ocamlc -o jeu utilitaire.cmo affichage.cmo joueur.cmo ordi_hasard.cmo ordi_intelligent.cmo jeu.cmo

./jeu [insérer le nom du mode de jeu]

(voir le rapport pour plus de précisions)
