(*Le code d'affichage de la grille est fini (modulo des changements minimes peut être au fur et à mesure)
 actuellement je fais la fonction jouer pour l'utilisateur qui est quasiment fini, il ne manque qu'un dico de test *)

type lettre = Mauvaise | Bof | Bonne;;


let verification (mot_a_tester : string) (mot_mystere : string) (longueur_mot : int) : lettre array =
  let tab_reponse = Array.make longueur_mot Bonne in (*initialisation du tableau qui sera renvoyé, par défaut toutes les lettres sont bien placées*)
  let tab_mystere = Array.make longueur_mot '*' in (*tableau qui servira à "recopier" le mot mystere, afin de travailler plus simplement dessus*)
  
  (*recopiage du mot mystere dans le tableau mystere*)
  for i = 0 to longueur_mot -1 do 
    tab_mystere.(i) <- mot_mystere.[i]
  done; 

  (*détermination des lettres bien placées ; on ne change rien dans le tableau réponse car il est déjà initialisé à Bonne*)
  for i = 0 to longueur_mot -1 do 
    if mot_a_tester.[i] = mot_mystere.[i] 
    then tab_mystere.(i) <- '*' (*dans le tableau du mot mystere, on remplace le caractère trouvé par un autre caractère (voir suite pour utilité)*)
  done; 

  (*détermination des autres lettres : existantes mais mal placées, ou tout simplement pas dans le mot*)
  for i = 0 to longueur_mot -1 do 
    if mot_a_tester.[i] <> mot_mystere.[i] then (*on occulte le cas où on a les bonnes lettres*)
      (*explication du code : on veut que les lettres "mal placées" soit repérées. Ainsi, on regarde si la lettre en position i du mot à tester
        se situe dans le mot mystère, et on récupère alors l'indice (j) dans le tableau mystere de cette lettre. Cela permettra de changer la
        lettre plus tard dans ce même tableau. En effet, si dans le mot à tester, il y a 3 fois la même lettre mais qu'elle n'apparaît que
        deux fois dans le mot mystère, il ne faudra pas afficher 3 fois "Bof" mais seulement 2 fois, et la dernière sera donc mauvaise. 
        Ainsi, une fois qu'on a trouvé une lettre, on modifie celle du tableau pour "ne plus retomber dessus" *)
      let j = ref 0 in 
      while !j < longueur_mot && tab_mystere.(!j) <> mot_a_tester.[i]  do
        incr j
      done;
      if !j <> longueur_mot (*si on a bien trouvé cette lettre*)
      then begin tab_reponse.(i) <- Bof ; tab_mystere.(!j) <- '*' end (*alors on ajoute le fait que le la lettre soit presque bonne dans le tableau renvoyé, et on change le caractère dans le tableau mystere*)
      else tab_reponse.(i) <- Mauvaise (*sinon, elle n'est pas dans le mot*)
  done;
  tab_reponse;;


exception Trouve ;;

(*fonction de rehcerche dichotomique dans un tableau pour avoir une complexité en ln (n)*)
let dichotomie (tab : 'a array) (x : 'a) : bool=
  try
    let gauche, droite = ref 0, ref (Array.length tab) in
    while !gauche < !droite do
      let milieu = (!gauche + !droite) / 2 in
      if tab.(milieu) = x 
      then raise Trouve ; (*on a trouvé l'élément recherché, on lève une exception pour interrompre la recherche*)
      if tab.(milieu) > x (*cas où l'élément recherché est "à gauche" de l'élément central*)
      then droite := milieu 
      else gauche := milieu+1(*cas où l'élément recherché est "à droite" de l'élément central*)
    done ;
    false
  with Trouve ->true
    




(*fonction d'affichage qui affiche un mot dans le tableau de jeu motus en fonction de l'état de ses lettres*)
(*exemple : | c | h | a | t | 
            | * | ° |   | ° |  *)
let afficher_mot (mot : string * lettre array) (nb_lettres : int) = 
  assert (nb_lettres >= 0 );
  let chaine, etats = mot in (*on récupère respectivement le mot en lui même et l'état de ses lettres*)
  for cpt = 0 to (nb_lettres-1) do
    
    print_string "| " ;
    print_string (String.sub chaine cpt 1) ;
    print_string " "
  done;
  print_endline "|";

  (*affichage de l'état de ses lettres*)
  (*NB : ° -- lettre contenue dans le mot mais mal placée
         * -- lettre bien placée et a fortiori contenue dans le mot 
           -- lettre non contenue dans le mot mystère *)
  for cpt = 0 to (nb_lettres-1) do 
    print_string "| " ;
    if (etats.(cpt) = Bof)
    then print_string "°"
    else if (etats.(cpt) = Bonne)
    then print_string "*"
    else print_string " " ;
    print_string " "
  done;
  print_endline "|"

;;

(*fonction auxiliaire qui crée une ligne d'affichage (en string) de motus vide dans le but d'améliorer la complexité
exemple : "|   |   |   |" *)
let creer_ligne_vide (nb_lettres : int) : string =
  assert (nb_lettres >= 0);
(*une telle chaine est en réalité une juxtaposition de blocs "|  " et d'un "|" final*)
  let brique = "|   " and chaine = ref "|" in
  for cpt = 0 to (nb_lettres -1) do
    print_int cpt;
    chaine := brique ^ !chaine
  done;
  !chaine
  
;;

(*affichage global de la grille en fonction du nombre d'essai, de la taille des mots et du nombres de mots déjà proposés*)
let afficher_grille (nb_lettres : int) (nb_essai_restant : int) (res : (string * lettre array) array): unit =
  if (res <> ([| |]) ) then
    assert ((String.length (fst(res.(0))) = nb_lettres));
(*on crée une ligne simple dans le but d'améliorer la complexité ex : "------------"  *)
  let ligne = String.make (nb_lettres * 4 + 2) '-' in
  (*on crée une ligne vide ex : "|   |   |   |   |" *)
  let ligne_vide = creer_ligne_vide nb_lettres in 
  let nb_mots = Array.length res in
  print_endline "MO MO MO MOTUS !!!!";
  print_newline (); 
  (*disjonction de cas en fonction de la vacuité du tableau de mots déjà proposés*)
  if (nb_mots = 0)
  then
    begin
    (*si le tableau est vide, alors la grille n'est qu'une succession de ligne et de ligne_vide*)
      for cpt = 0 to (nb_essai_restant-1) do
        print_int cpt;
        print_endline ligne_vide;
        print_endline ligne_vide;
        print_endline ligne; 
      done;
    end
  else
    begin
    (*si le tableau n'est pas vide on procède ne deux temps : 
    on commence par afficher les mots avec la fonction définie plus haut
    puis on ajoute autant de lignes vides qu'il ne reste d'essais au joueur*)
      for cpt = 0 to (nb_mots -1) do 
        let mot = res.(cpt) in
        afficher_mot mot nb_lettres; 
        print_endline ligne;
        
      done; 
      for cpt = 0 to (nb_essai_restant-1) do 
        print_int cpt;
        print_endline ligne_vide;
        print_endline ligne_vide;
        print_endline ligne;
        
      done;
    end ;;


(*Fonction d'affichage informant le joueur de sa victoire : il a trouvé le mot mystère*)
let gagner (mot_a_trouver : string) (nb_essais_restants : int) : unit =
  print_endline "Saperlipopette! Tu as gagné!!";
  print_endline "Bravoooooo tu as trouvé le nombre mystère qui était " ;
  print_string mot_a_trouver;
  print_endline " !!!!!!";
  print_string "Et en plus, il te restait encore ";
  print_int (nb_essais_restants +1);
  print_endline " essai(s) !!!" ;

;;

(*Fonction d'affichage pour informer le joueur qu'il a perdu : il n'a pas trouvé le mot mystère malgrés les essais dont il disposait*)
let perdu (mot_a_trouver : string) : unit = 
  print_endline "Mouhahahahahaha tu as perdu !!!";
  print_endline "Je savais bien que j'étais trop fort pour toi....." ;
  print_endline "Dans mon infinie bonté, je vais te révéler le mot mystère...";
  print_string "C'était .... " ;
  print_endline mot_a_trouver;
  print_endline "Pas trop déçu? Avec quelques essais en plus, tu aurais peut-être réussi..."
    
;;

exception Break;;

(*fonction de formatage de mot :
on regarde s'il y a d'autres caractères que des lettres sans accents*)
(*on permet à l'utilisateur de rentrer des majuscules ou des minuscules selon son bon vouloir*)
let formate_mot (mot : string) : string = 
(*passage du mot en majuscules*)
  let mot_bis = String.uppercase_ascii mot in
  let alpha = [| 'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'; 'I'; 'J';'K';'L';'M';'N';'O';'P';'Q';'R';'S';'T';'U';'V';'W';'X';'Y';'Z'|] in
  try
    for cpt = 0 to (String.length mot -1) do 
    (*on regarde si chaque lettre fait partie des lettres autorisées*)
      if not(dichotomie alpha mot_bis.[cpt] ) 
      (*si ce n'est pas le cas on lève une exception qui va renvoyer un mot spécial de longueur 2 (qui nepeut donc pas être proposé par l'utilisateur car le mot_secret peut faire de 3 à 7 lettres*)
      then raise Break;
    done;
    mot_bis
  with Break -> "zz"

(*easter egg voulu par un des membre du groupe (je vous laisse deviner lequel!) qui renvoie une petite surprise!*)
let easter_egg (mot : string) : unit = 
  if mot = "DUNE"
  then print_endline "oh mais incroyable, ferais tu partie de cette caste d'initiés qui connaissent cet incroyable, que dis-je divin, film!!!!";
  print_endline "VIVE DUNE !!!!!!!";
  print_endline "Cela vaut bien une petite victoire en plus sapristi !!!!;"
  
;;
(*fonction de jeu proprement dite prenant en paramètre le nombre d'essais voulu par l'utilisateur*)
let jouer (nb_essais : int) (dictionnaire : string array array) : unit =
  assert (not((Array.length dictionnaire) = 0));
(*on tire aléatoirement la longueur du mot_secret*)
  let lg = (Random.int 4) +3  in
  (*on tire aleéatoirement un mot parmi les mots de longeur choisie plus haut*)
  print_int lg;
  print_endline "   que pasa";
  let mot_a_trouver = dictionnaire.(lg-3).(Random.int (Array.length dictionnaire.(lg-3))) in
  print_endline mot_a_trouver;
  (*variable qui va prendre en paramêtre successivment les mots proposés pas le joueur*)
  let mot_proposer = ref "" in
  (*variable qui va contenir peu à peu tous les mots  proposés par le joueur ainsi que leurs étatsrespectifs*)
  let tab_etats = ref [| |] in
  print_endline "holaa";
  afficher_grille lg nb_essais ([| |]);
  print_string "le mot que tu dois découvrir est en ";
  print_int lg;

  print_endline "lettres";
  let cpt = ref 1 in 
  (*on continue à demander des mots à l'utilisateur tant qu'il a encore des essais, qu'il n'a trouvé ni l'easter egg ni le mot_mystere*)
  while nb_essais >= !cpt && (!mot_proposer <> mot_a_trouver) && (!mot_proposer <> "DUNE") do
    print_endline "Nb : ° - lettre contenue dans le mot mystère mais mal placée, * - lettre bien placée ";
    print_endline "Quel mot me proposes tu?"; 
    mot_proposer := read_line() ;
    mot_proposer := (formate_mot !mot_proposer);
    (*on vérfie que le mot proposé est de la bonne longueur*)
    if ((String.length !mot_proposer) <> lg) 
    then 
      begin 
        print_endline "Le mot que tu as passé en paramètre n'est pas de la bonne taille!";
        
      end
    (*on vérifie ensuite que le mot proposé ne comporte pas de caractères spéciaux *)
    else if !mot_proposer = "zz"
    then print_endline "ce mot comporte des caractères spéciaux interdits !! "
    

    (*si les deux conditions ci-dessus sont respéctées, on regarde si le mot existe (ie s'il est sontenu dans la ase de donnée)*)
    else if (not( dichotomie  dictionnaire.(lg-3) !mot_proposer))
      then print_endline "ce mot n'existe pas!"
      else
      (*si toutes les conditions ci-dessus sont respéctées, on regarde l'état de chaque lettre*)
      let etat = (!mot_proposer , verification !mot_proposer mot_a_trouver lg) in
      begin 
        tab_etats := Array.append !tab_etats [|etat|];
        afficher_grille lg (nb_essais- (!cpt)) !tab_etats;
        cpt := !cpt +1;
      end
  done; 
  (*si le joueur a trouvé l'easter egg*)
  if (!mot_proposer = "DUNE")
  then easter_egg "DUNE"
  (*cas où l'utilisateur a soit trouvé le mot_secret, dans ce cas c'est gagné, sinon c'est qu'il a épuisé toutes ses tentatives, il a perdu *)
  else if (!mot_proposer <> mot_a_trouver)
  then 
    begin perdu mot_a_trouver end
  else begin gagner mot_a_trouver (nb_essais - !cpt) end ;;

(*fonction d'affichage d'accueil du joueur qui lui permet de choisir le nombre d'essais qu'il veut avoir *)
let debut (dictionnaire : string array array) = 
  assert (not(Array.length dictionnaire = 0));
  print_endline "MO....MO....MO....MOTUS !!!";
  print_endline "Bonjour cher joueur!!";
  print_endline "Tu vas pouvoir tenter, de découvrir le mot secret... ";
  print_endline "Note de jeu : les mots doivent être entrés formatés (ie sans accents en tout genre)";
  print_endline "Dis moi, combien d'essais veux-tu avoir? ";
  print_endline "Tu peux en avoir entre 3 si tu te sens tel .. ....";
  print_endline ".. Jusqu'à 10 si la sureté de ... te correspond plus";
  print_endline "Alors, quel est ton choix?" ;
  print_endline "Entre un chiffre entre 3 et 15 pour choisir ton nombre d'essai";
  

  let nb_essais = ref (read_int ()) in
  assert ( !nb_essais <= 10 && 3 <= !nb_essais);
  print_endline "Que la partie comenceeeee!";
  
  (*l'utilisateur peut rejouer après chaque partie, et peut même changer de nombre de tentative qi l'envie le prend!*)
  jouer !nb_essais dictionnaire ;
  print_endline "veux tu encore jouer? (tape OUI ou NON)";
  let choix = ref (read_line ()) in
  begin
  print_endline "Combien veux-tu d'essais cette fois-ci? (tape un nombre entre 3 et 15";
  nb_essais := (read_int ()) ;
  end;
  while !choix <> "NON" do
    if !choix = "OUI" 
    then 
    begin jouer !nb_essais dictionnaire;
    print_endline "veux tu encore jouer? (tape OUI ou NON)";
    choix := (read_line ());
    print_endline "Combien veux-tu d'essais cette fois-ci? (tape un nombre entre 3 et 15)";
    nb_essais := (read_int ()) ;

    end
  done;
;;


debut ( dictionnaire)
    
    
