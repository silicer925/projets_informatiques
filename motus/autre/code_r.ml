type lettre = Mauvaise | Bof | Bonne;;
let min_longueur = 3;;
let max_longueur = 7;; 
Random.self_init()

let dico_choix = [| [| "AMI" ; "ARC" ; "BAS" ; "COL" ; "COU" ; "DIX" ; "DOS" ; "DUR" ; "EAU" ; "EAU" ; "FEE" ; "FEU" ; "FIL" ; "FOI" ; "FOU" ; "ICI" ; "JEU" ; "JEU" ; "LIT" ; "MAL" ; "MOT" ; "MUR" ; "MUR" ; "NOM" ; "PAS" ; "PEU" ; "PLI" ; "POT" ; "ROI" ; "SAC" ; "SEC" ; "SIX" ; "SOL" ; "SUR" ; "TAS" ; "TOT" ; "USE" ; "VIS" |][| "BANC" ; "BIEN" ; "BLEU" ; "BOIS" ; "BORD" ; "BOUT" ; "BRAS" ; "BRUN" ; "CAGE" ; "CERF" ; "CINQ" ; "CLEF" ; "CLOU" ; "COIN" ; "COTE" ; "COUP" ; "COUR" ; "CUBE" ; "DAME" ; "DANS" ; "DEUX" ; "DIRE" ; "DOUX" ; "EPEE" ; "FACE" ; "FILE" ; "FILM" ; "FOIS" ; "FOND" ; "FORT" ; "GANT" ; "GARE" ; "GRIS" ; "GROS" ; "GRUE" ; "HAUT" ; "HUIT" ; "IDEE" ; "JEAN" ; "JOLI" ; "JUPE" ; "LIRE" ; "LOIN" ; "LONG" ; "MAIN" ; "MEME" ; "MINE" ; "MOTO" ; "MUET" ; "NEUF" ; "NOIR" ; "PAGE" ; "PAIX" ; "PATE" ; "PEUR" ; "PIED" ; "PIED" ; "PION" ; "PLUS" ; "PNEU" ; "POLI" ; "PONT" ; "PONT" ; "PRES" ; "QUAI" ; "RANG" ; "ROBE" ; "ROND" ; "ROND" ; "ROUE" ; "ROUX" ; "SAGE" ; "SALE" ; "SAUT" ; "SCIE" ; "SEAU" ; "SENS" ; "SEPT" ; "SEUL" ; "SOUS" ; "TARD" ; "TETE" ; "TOUR" ; "TROP" ; "VELO" ; "VERS" ; "VERT" ; "VITE" ; "VOIX" ; "ZERO" |][| "ABIME" ; "AIDER" ; "ALBUM" ; "ALLER" ; "ANGLE" ; "APPEL" ; "APRES" ; "ARRET" ; "ASSIS" ; "AVANT" ; "AVION" ; "BALLE" ; "BANDE" ; "BARBE" ; "BARRE" ; "BILLE" ; "BLANC" ; "BLOND" ; "BOITE" ; "BOITE" ; "BOTTE" ; "BRUIT" ; "CALME" ; "CARRE" ; "CARTE" ; "CASSE" ; "CHAUD" ; "CHOSE" ; "CHUTE" ; "CLAIR" ; "COEUR" ; "COLLE" ; "COLLE" ; "CONTE" ; "CORDE" ; "CORPS" ; "COUDE" ; "COURT" ; "CRAIE" ; "CRIER" ; "DOIGT" ; "DROIT" ; "ECOLE" ; "ECRAN" ; "ELEVE" ; "ENGIN" ; "EPAIS" ; "FAIRE" ; "FAUTE" ; "FESSE" ; "FILET" ; "FILLE" ; "FINIR" ; "FONCE" ; "FORME" ; "FREIN" ; "FROID" ; "FUSEE" ; "FUSIL" ; "GARER" ; "GENER" ; "GENOU" ; "GOMME" ; "GRAND" ; "HABIT" ; "IMAGE" ; "JAMBE" ; "JAUNE" ; "JOUER" ; "JOUET" ; "LACER" ; "LACET" ; "LAINE" ; "LARGE" ; "LAVER" ; "LEVER" ; "LIGNE" ; "LINGE" ; "LISSE" ; "LISTE" ; "LITRE" ; "LIVRE" ; "MAGIE" ; "MAINS" ; "METAL" ; "METRE" ; "MICRO" ; "MIEUX" ; "MOINS" ; "MOYEN" ; "NAGER" ; "NOEUD" ; "OBEIR" ; "OBJET" ; "ONGLE" ; "ORDRE" ; "OUTIL" ; "PAIRE" ; "PANNE" ; "PELLE" ; "PENTE" ; "PERLE" ; "PETIT" ; "PHOTO" ; "PLIER" ; "PLUIE" ; "POCHE" ; "POING" ; "POINT" ; "POMPE" ; "PORTE" ; "POSER" ; "POSER" ; "POUCE" ; "PREAU" ; "PRISE" ; "PUNIR" ; "RADIO" ; "RAMPE" ; "RATER" ; "RAYON" ; "REINE" ; "ROUGE" ; "RUBAN" ; "SABLE" ; "SALIR" ; "SALLE" ; "SALON" ; "SERRE" ; "SIEGE" ; "SIGNE" ; "SIGNE" ; "SINGE" ; "SOURD" ; "SPORT" ; "STYLO" ; "TABLE" ; "TACHE" ; "TALON" ; "TAPER" ; "TAPIS" ; "TASSE" ; "TENIR" ; "TIRER" ; "TISSU" ; "TITRE" ; "TORDU" ; "TRAIN" ; "TRAIT" ; "TRIER" ; "TROIS" ; "TROUS" ; "TUYAU" ; "VENIR" ; "VERRE" ; "VESTE" ; "VIDER" ; "VITRE" ; "VOLER" ; "WAGON" |][| "ABSENT" ; "ADROIT" ; "AGITER" ; "AMENER" ; "AMUSER" ; "ANCIEN" ; "ANORAK" ; "AUTANT" ; "AUTOUR" ; "BAGAGE" ; "BALLON" ; "BATEAU" ; "BATTRE" ; "BONDIR" ; "BONNET" ; "BOUDER" ; "BOUGER" ; "BOUTON" ; "BULLES" ; "BUREAU" ; "CABANE" ; "CACHER" ; "CAHIER" ; "CAISSE" ; "CAMION" ; "CANARD" ; "CARNET" ; "CARTON" ; "CARTON" ; "CASIER" ; "CASQUE" ; "CASSER" ; "CHAINE" ; "CHAISE" ; "CHAISE" ; "CLASSE" ; "COGNER" ; "COLERE" ; "COLLER" ; "CONTRE" ; "COPAIN" ; "COPIER" ; "COQUIN" ; "COUDRE" ; "COULER" ; "COUPER" ; "COURIR" ; "COURSE" ; "CRAYON" ; "CUISSE" ; "DANGER" ; "DANSER" ; "DEBOUT" ; "DEBOUT" ; "DEDANS" ; "DEHORS" ; "DESSIN" ; "DESSUS" ; "DEVANT" ; "DOIGTS" ; "DOMINO" ; "DONNER" ; "DORMIR" ; "DOUCHE" ; "DROITE" ; "ECRIRE" ; "EFFORT" ; "ENCORE" ; "ENFANT" ; "ENTRER" ; "ENTRER" ; "EPAULE" ; "EQUIPE" ; "ETROIT" ; "FACILE" ; "FERMER" ; "FEUTRE" ; "FLAQUE" ; "FLECHE" ; "GAGNER" ; "GARAGE" ; "GARCON" ; "GARDER" ; "GAUCHE" ; "GENTIL" ; "GOUTER" ; "GROUPE" ; "HANCHE" ; "HUMIDE" ; "HURLER" ; "IMITER" ; "INTRUS" ; "JALOUX" ; "JAMAIS" ; "JOUEUR" ; "LANCER" ; "LAVABO" ; "LETTRE" ; "MADAME" ; "MAITRE" ; "MANCHE" ; "MARCHE" ; "METTRE" ; "MEUBLE" ; "MILIEU" ; "MODELE" ; "MOITIE" ; "MONTER" ; "MOTEUR" ; "MOUFLE" ; "MOULIN" ; "MOUSSE" ; "MUSCLE" ; "NOMBRE" ; "NUMERO" ; "OUVRIR" ; "OUVRIR" ; "PAPIER" ; "PARDON" ; "PAREIL" ; "PARLER" ; "PARTIE" ; "PARTIR" ; "PARTIR" ; "PATTES" ; "PEDALE" ; "PERCER" ; "PERDRE" ; "PILOTE" ; "PINCER" ; "PLATRE" ; "PLIAGE" ; "POINTU" ; "PORTER" ; "POSTER" ; "POUTRE" ; "PRENOM" ; "PRETER" ; "PRINCE" ; "PRIVER" ; "PROPRE" ; "PUZZLE" ; "PYJAMA" ; "QUATRE" ; "RAMPER" ; "RANGER" ; "RAYURE" ; "RESTER" ; "RETARD" ; "RIDEAU" ; "ROULER" ; "SAUTER" ; "SECHER" ; "SENTIR" ; "SERRER" ; "SERVIR" ; "SIESTE" ; "SOLDAT" ; "SOLIDE" ; "SONNER" ; "SORTIE" ; "SORTIR" ; "SOUPLE" ; "SUIVRE" ; "TAILLE" ; "TAMPON" ; "TENDRE" ; "TIROIR" ; "TOMBER" ; "TRICOT" ; "TROUER" ; "TUNNEL" ; "VALISE" ; "VENTRE" ; "VERSER" ; "VIRAGE" ; "VOLANT" ; "VOYAGE" ; "ZIGZAG" |][| "AFFAIRE" ; "AFFICHE" ; "AMPOULE" ; "AMUSANT" ; "APPUYER" ; "APPUYER" ; "ARMOIRE" ; "ARRETER" ; "ARRIERE" ; "ARRIVER" ; "ARROSER" ; "AVANCER" ; "BAGARRE" ; "BAIGNER" ; "BAILLER" ; "BAISSER" ; "BARREAU" ; "BOUCHER" ; "BOUCHON" ; "CABINET" ; "CAGOULE" ; "CARREAU" ; "CERCEAU" ; "CHANGER" ; "CHANSON" ; "CHANTER" ; "CHAPEAU" ; "CHARGER" ; "CHATEAU" ; "CHEMISE" ; "CHIFFRE" ; "CHOISIR" ; "CISEAUX" ; "COLLANT" ; "COMPTER" ; "COUCHER" ; "COULEUR" ; "COULOIR" ; "COUVRIR" ; "CRACHER" ; "CRAVATE" ; "CREUSER" ; "CROCHET" ; "CULOTTE" ; "CURIEUX" ; "CUVETTE" ; "DECORER" ; "DEMOLIR" ; "DERNIER" ; "DISPUTE" ; "DOSSIER" ; "DOUCHER" ; "ECARTER" ; "ECHARPE" ; "ECHASSE" ; "ECHELLE" ; "ECOUTER" ; "ECRASER" ; "EFFACER" ; "EMMENER" ; "ENERVER" ; "ENFILER" ; "ENLEVER" ; "ENVOLER" ; "ENVOYER" ; "ENVOYER" ; "ESSUYER" ; "ETAGERE" ; "FATIGUE" ; "FENETRE" ; "FEUILLE" ; "FICELLE" ; "FLOTTER" ; "FRAPPER" ; "GALOPER" ; "GARDIEN" ; "GLISSER" ; "GONFLER" ; "GOUTTES" ; "GRIMPER" ; "GRONDER" ; "INONDER" ; "JONGLER" ; "LAISSER" ; "MAILLOT" ; "MANQUER" ; "MANTEAU" ; "MARCHER" ; "MARTEAU" ; "MATELAS" ; "MECHANT" ; "MESURER" ; "MODELER" ; "MONTRER" ; "MONTRER" ; "MORCEAU" ; "MOUILLE" ; "MUSIQUE" ; "NOUVEAU" ; "OBLIGER" ; "PARKING" ; "PEDALER" ; "PEINDRE" ; "PELUCHE" ; "PENCHER" ; "PERCHER" ; "PINCEAU" ; "PINCEAU" ; "PLACARD" ; "PLAFOND" ; "PLANCHE" ; "PLEURER" ; "PLONGER" ; "POIGNET" ; "POISSON" ; "POUSSER" ; "POUVOIR" ; "PREMIER" ; "PRENDRE" ; "PRESENT" ; "PRESQUE" ; "PRESSER" ; "PRESSER" ; "PROGRES" ; "QUITTER" ; "RECITER" ; "RECULER" ; "REFUSER" ; "REMPLIR" ; "REPARER" ; "REPETER" ; "REUSSIR" ; "REVENIR" ; "RIVIERE" ; "ROBINET" ; "ROULADE" ; "SAIGNER" ; "SEMELLE" ; "SEPARER" ; "SERIEUX" ; "SERPENT" ; "SERRURE" ; "SIFFLER" ; "SIFFLET" ; "SILENCE" ; "SOMMEIL" ; "SOURIRE" ; "SOUVENT" ; "SUIVANT" ; "TABLEAU" ; "TABLIER" ; "TAILLER" ; "TAMBOUR" ; "TOUCHER" ; "TOURNER" ; "TRAINER" ; "TRAVAIL" ; "TREMPER" ; "TROUSSE" ; "TROUVER" ; "TROUVER" ; "VITESSE" ; "VOITURE" ; "VOULOIR" |] |]
let longueur_dico_choix = Array.length dico_choix

let verification (mot_a_tester : string) (mot_mystere : string) (longueur_mot : int) : lettre array =
  let tab_reponse = Array.make longueur_mot Bonne in (*initialisation du tableau qui sera renvoyé, par défaut toutes les lettres sont bien placées*)
  let tab_mystere = Array.make longueur_mot '*' in (*tableau qui servira à "recopier" le mot mystere, afin de travailler plus simplement dessus*)
  
  (*recopiage du mot mystere dans le tableau mystere*)
  for i = 0 to longueur_mot -1 do 
    tab_mystere.(i) <- mot_mystere.[i]
  done; 

  (*détermination des lettres bien placées ; on ne change rien dans le tableau réponse car il est déjà initialisé à Bonne*)
  for i = 0 to longueur_mot -1 do 
    if mot_a_tester.[i] = mot_mystere.[i] 
    then tab_mystere.(i) <- '*' (*dans le tableau du mot mystere, on remplace le caractère trouvé par un autre caractère (voir suite pour utilité)*)
  done; 

  (*détermination des autres lettres : existantes mais mal placées, ou tout simplement pas dans le mot*)
  for i = 0 to longueur_mot -1 do 
    if mot_a_tester.[i] <> mot_mystere.[i] then (*on occulte le cas où on a les bonnes lettres*)
      (*explication du code : on veut que les lettres "mal placées" soit repérées. Ainsi, on regarde si la lettre en position i du mot à tester
        se situe dans le mot mystère, et on récupère alors l'indice (j) dans le tableau mystere de cette lettre. Cela permettra de changer la
        lettre plus tard dans ce même tableau. En effet, si dans le mot à tester, il y a 3 fois la même lettre mais qu'elle n'apparaît que
        deux fois dans le mot mystère, il ne faudra pas afficher 3 fois "Bof" mais seulement 2 fois, et la dernière sera donc mauvaise. 
        Ainsi, une fois qu'on a trouvé une lettre, on modifie celle du tableau pour "ne plus retomber dessus" *)
      let j = ref 0 in 
      while !j < longueur_mot && tab_mystere.(!j) <> mot_a_tester.[i]  do
        incr j
      done;
      if !j <> longueur_mot (*si on a bien trouvé cette lettre*)
      then begin tab_reponse.(i) <- Bof ; tab_mystere.(!j) <- '*' end (*alors on ajoute le fait que le la lettre soit presque bonne dans le tableau renvoyé, et on change le caractère dans le tableau mystere*)
      else tab_reponse.(i) <- Mauvaise (*sinon, elle n'est pas dans le mot*)
  done;
  tab_reponse;;


let tableau_creation_affichage (nb_lettres : int) : string * lettre array =
  (* fonction permettant de créer un tableau d'affichage, auquel sera ajouté les différents essais faits par les ordis, afin d'afficher
     le résultat à la fin *)
  let chaine = ref "" in
  let tab = Array.make nb_lettres Mauvaise in (*on met "Mauvaise" comme valeur de base, afin d'afficher un vide dans l'affichage si on est pas allé au bout des essais*)
  for i = 0 to nb_lettres -1 do (*on remplie la chaîne d'espaces pour afficher des trous dans la grille*)
    chaine := !chaine ^ " "
  done;
  (!chaine,tab);;

let partie_ordi_debile (dictionnaire_pioche : string array array) : unit =
  (* fonction simulant une partie jouée par un ordinateur, qui jouerait des mots au hasard *)
  let pioche_mystere = dico_choix.(Random.int longueur_dico_choix) (*le dico de choix est un string array array : on récupère d'abord un des tableaux de longueurs au hasard*)
  let mot_mystere = pioche_mystere.(Random.int (Array.length pioche_mystere)) in (*On choisit le mot mystère dans la petite base de donnée*)
  let longueur_mot_mystere = String.length mot_mystere in 
  let nombre_essais = ref 6 in (*On fixe le nombre d'essais à 6 (car de toute manière à moins d'avoir de la chance il est peu probable qu'il réussisse)*)
  
  let sous_tableau_affichage = tableau_creation_affichage longueur_mot_mystere in (*nom trompeur : est en réalité un couple string * lettre array *)
  let tableau_affichage = Array.make !nombre_essais sous_tableau_affichage in (*on initialise le tableau d'afficchage : on y ajoutera tous les essais et leur vérification, pour tout afficher à la fin*)
  let longueur_tableau_affichage = !nombre_essais in 
  
  let dico_bonne_taille = dictionnaire_pioche.(longueur_mot_mystere - min_longueur) in (*dictionnaire_pioche étant un tableau de tableau, chacun contenant les mots de même taille, on récupère celui qui nous intéresse (ex si le mot mystère est de taille 5, les mots de longueur 5)*)
  let taille_dico = Array.length dico_bonne_taille in

  let essai = ref dico_bonne_taille.(Random.int (taille_dico)) in (*l'ordi réalise un premier essai en prenant un mot au hasard*)
  tableau_affichage.(0) <- (!essai,verification !essai mot_mystere longueur_mot_mystere); (*On ajoute à l'affichage ce premier essai et sa vérification*)
  decr nombre_essais; (*l'ordi a utilisé un essai, on décrémente donc le nombre d'essais*)

  while !nombre_essais > 0 && !essai <> mot_mystere do (*tant qu'il a des essais et qu'il n'a pas trouvé le mot*)
    essai := dico_bonne_taille.(Random.int (taille_dico)); (*il refait un essai*)
    tableau_affichage.(longueur_tableau_affichage - !nombre_essais) <- (!essai,verification !essai mot_mystere longueur_mot_mystere); (*on ajoute (dans l'ordre) les essais et leur verif*)
    decr nombre_essais; (*et on décrémente le nombre d'essais*)
  done; 

  afficher_grille longueur_mot_mystere 0 tableau_affichage; (*on affiche la grille finale*)
  if !essai <> mot_mystere (*on détermine si l'ordi a gagné ou nom*)
  then 
    begin
      print_endline "Quel dommage ! Le bot a perdu, il n'est pas très doué dis donc... Le mot mystère était : " ; 
      print_string mot_mystere
    end
  else print_endline "Incroyable, mais vrai ! Le bot a gagné !!!"


let occurence_string (mot : string) (lettre : char) : int = (*simple fonction retournant le nb d'occurence d'une lettre dans une chaîne*)
  let cpt = ref 0 in
  for i = 0 to (String.length mot) -1 do
    if mot.[i] = lettre then
      incr cpt
  done;
  !cpt;; 
  
let rec occurence_liste (liste : char list) (lettre : char) (cpt : int) : int = (*simple fonction retournant le nb d'occurence d'une lettre dans une liste de lettres*)
  match liste with
  | [] -> cpt
  | hd :: tl when hd = lettre -> occurence_liste tl lettre (cpt+1)
  | hd :: tl -> occurence_liste tl lettre cpt;;


exception Continue 
let note (dico : string array) (taille_dico : int) (mot : string) (verif : lettre array) (longueur_mot : int): int array =
  (* fonction permettant d'attribuer une "note" à un tableau de mot passé en paramètre. Chacun des mots se verra attribué un "1"
     ou un "0", en fonction de s'il est respectivement non compatible avec l'essai réalisé, ou compatible (ie 0 s'il est utile, 1 sinon)
     (NB : il sera souvent dit "on supprime du tableau" : par analogie avec la suite, cela signifie attribuer une note de 1 au mot)*)

  let tableau_notes = Array.make taille_dico 0 in (*on initialise le tableau des notes : au départ tous les mots sont bons*)
  for i = 0 to taille_dico -1 do (*on parcourt le dico en paramètre*)
    if dico.(i) = mot then (*on supprime du dico le mot que l'on vient de tester*)
      tableau_notes.(i) <- 1
    else
      try (* try/with permettant de gagner un peu de temps lors de l'exécution, avec l'exception Continue (si une des lettre du mot remplie une cdt de suppression, alors nul besoin de tester les autres)*)
        for j = 0 to longueur_mot -1 do (* on parcourt les lettres du mot (en paramètre, du dico, pas d'importance, ils font la même taille)*)
          if verif.(j) = Bonne then (*Si on a une lettre bien placée, alors si le mot du dico n'a pas la même lettre au même endroit, on le supprime du dico*)
            begin 
              if dico.(i).[j] <> mot.[j] then 
                begin tableau_notes.(i) <- 1 ; raise Continue end 
            end
          else if verif.(j) = Bof then (*Si une lettre est dans le mot mais n'est pas bien placée : alors on supprime les mots ayant cette lettre au même endroit, et ceux qui ne contiennent pas du tout cette lettre*)
            begin 
              if dico.(i).[j] = mot.[j] || not (String.contains dico.(i) mot.[j]) then
                begin tableau_notes.(i) <- 1 ; raise Continue end
            end
           else (*Cas le plus compliqué : si la lettre est mauvaise*)
            (* Plusieurs cas de figures se posent... Si la lettre n'est tout simplement pas dans le mot, ou si c'est une nouvelle occurence d'une lettre bien placée ou mal placée*)
            (* Afin de traiter au mieux ces cas de figures, nous devons d'abord récuperer les bonnes et bof lettres. Comme on ne sait pas combien il y en a, on fait cela dans une liste*)
            let liste_lettres_bonne_bof = ref [] in
            for k = 0 to longueur_mot -1 do
              if verif.(k) <> Mauvaise then
                liste_lettres_bonne_bof := mot.[k] :: !liste_lettres_bonne_bof
            done;
            (*on va ensuite s'occuper en premier du second cas : si la lettre mauvaise se retrouve plus de fois dans le mot du dico que l'on regarde que dans les bonnes ou bof lettres, alors cela veut dire qu'elle est de trop et qu'il faut donc supprimer ce mot*)
            if occurence_string dico.(i) mot.[j] > occurence_liste !liste_lettres_bonne_bof mot.[j] 0 then
              begin tableau_notes.(i) <- 1 ; raise Continue end
            else
              (*Sinon, occupons-nous du premier cas : la lettre n'est pas dans le mot. Alors dans ce cas, il faut retirer tous les mots qui la contiennent... En faisant attention de ne pas supprimer ceux qui la contiennent déjà à une place bonne ou bof ! *)
              let chaine_tmp = ref "" in (*pour cela on crée une nouvelle chaîne, dans laquelle on ne met que les lettres bof ou mauvaise, sans que celle-ci n'apparaissent dans la liste des bonnes ou mauvaises lettre*)
              for k = 0 to longueur_mot -1 do
                if verif.(k) <> Bonne && not (List.mem dico.(i).[k] !liste_lettres_bonne_bof) then
                  begin chaine_tmp := !chaine_tmp ^ (String.make 1 dico.(i).[k]) end
              done; 
              (*enfin, on regarde si cette nouvelle chaîne contient la lettre mauvaise : si oui, on la supprime*)
              if String.contains !chaine_tmp mot.[j] then
                begin tableau_notes.(i) <- 1 ; raise Continue end
        done;
      with Continue -> ()
  done;
  tableau_notes;;
  
  
let decoupage_dico (dico : string array) (taille_dico : int) (mot : string) (verif : lettre array) (longueur_mot : int): string array = 
  (*fonction qui va venir "découper" (entendre réduire) un tableau de chaîne de carcatères, en ne gardant que celle dont la note est 0*)
  (*Comme on ne sait pas à l'avance combien de mots vont vérifier cette condition, on utilise une liste.*)
  let tableau_notes = note dico taille_dico mot verif longueur_mot in (*on récupère le tableau des notes du dico*)
  let liste_tmp = ref [] in
  for i = 0 to taille_dico -1 do (*pour chaque mot, si sa note est 0, alors on le garde et donc on le met dans la liste créée*)
    if tableau_notes.(i) = 0 then 
      liste_tmp := dico.(i) :: !liste_tmp
  done;
  Array.of_list !liste_tmp;;(*On tranfsorme la liste en tableau, afin de pouvoir continuer à le manipuler par la suite*)

let partie_ordi_intelligent (dictionnaire_pioche : string array array) : unit =
  (*fonction simulant une partie de motus jouée par un ordinateur plutôt intelligent : il se souvient des coups précédents et réduit sans cesse le champ des possibilités en réduisant le tableau dans lequel il fait ses essais*)
  let pioche_mystere = dico_choix.(Random.int longueur_dico_choix) (*le dico de choix est un string array array : on récupère d'abord un des tableaux de longueurs au hasard*)
  let mot_mystere = pioche_mystere.(Random.int (Array.length pioche_mystere)) in (*On choisit le mot mystère dans la petite base de donnée*)
  let longueur_mot_mystere = String.length mot_mystere in 
  let nombre_essais = ref 8 in (*nombre arbitraire, pouvant être changé. On lui donne deux essais de plus pour les cas où le mot est une variante d'autres mots très ressemblant (ex : Serie/Serge/Serre/Serbe/Serpe etc)*)

  let sous_tableau_affichage = tableau_creation_affichage longueur_mot_mystere in (*de même que pour le bot non-intelligent*)
  let tableau_affichage = Array.make !nombre_essais sous_tableau_affichage in
  let longueur_tableau_affichage = !nombre_essais in
  
  let dico_bonne_taille = ref dictionnaire_pioche.(longueur_mot_mystere -min_longueur) in (*de même, on récupère le dico avec les bonne taille. Cette fois-ci, on en fait une référence, car il évoluera (diminuera) au fur et à mesure des découpages*)
  let essai = ref !dico_bonne_taille.(Random.int (Array.length !dico_bonne_taille)) in (*il fait un essai, et l'ajoute après au tableau d'affichage*)
  tableau_affichage.(0) <- (!essai,verification !essai mot_mystere longueur_mot_mystere);
  decr nombre_essais;

  while !nombre_essais > 0 && !essai <> mot_mystere do (*tant qu'il reste des essais ou qu'il n'a pas trouvé le mot*)
    let verification_mot = verification !essai mot_mystere longueur_mot_mystere in (*on vérifie l'essai (on le met dans une varibale car on s'en sert plusieurs fois dans la boucle)*)
    dico_bonne_taille := decoupage_dico !dico_bonne_taille (Array.length !dico_bonne_taille) !essai verification_mot longueur_mot_mystere; (*on découpe le dico actuel*)
    essai := !dico_bonne_taille.(Random.int (Array.length !dico_bonne_taille)); (*on refait un nouvel essai*)
    tableau_affichage.(longueur_tableau_affichage - !nombre_essais) <- (!essai,verification_mot);(*on ajoute au tableau d'affichage*)
    decr nombre_essais; (*il perd un essai*)
  done; 
  afficher_grille longueur_mot_mystere 0 tableau_affichage;(*on affiche la grille finale*)
  if !essai <> mot_mystere 
  then 
    begin 
      print_endline "COMMENT ??? Ce n'est pas possible, je n'ai pas pu perdre... Le mot mystère était : " ; 
      print_string mot_mystere
    end
  else print_endline "Hahaha, trivial comme dirait l'autre, encore une victoire pour moi";;
