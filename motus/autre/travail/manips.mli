(* Fichier d'interface (très certainement inutile) de manips.ml *)

(** main
    - description : - Affiche les bornes de longueur des mots de notre petit dico
                    - Vérifie que les mots de la petite base se trouvent bien dans la grande.
                    - Trie nos tableaux de données en les renvoyant dans des fichiers .txt
	- entrée : aucune
	- précondition : les préconditions sont déjà vérifiées dans la fonction
	- sortie : unit (réalise des affichages et créé deux fichiers .txt)
	- postcondition : aucun effet de bord, créé deux fichiers .txt dans le répertoire courant
*)
val main : unit -> unit