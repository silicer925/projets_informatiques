
let longueur_extremum (tab : string array) : int*int =
  assert(Array.length tab <> 0);
  let long_max = ref (String.length tab.(0))  in             (*0n initialise nos valeurs de longueurs avec celle du premier mot.*)    
  let long_min = ref (String.length tab.(0)) in 
  for i = 1 to (Array.length tab) -1 do             (*Pour tous les mots contenus dans la base entrée.*)
    if String.length tab.(i) < !long_min then       (*Cas où la longueur d'un mot de la base est inférieur au minimum actuellement détecté.*)
      long_min := String.length tab.(i)
    else if String.length tab.(i) > !long_max then  (*Idem pour l'autre cas.*)
      long_max := String.length tab.(i)
  done;
  (!long_min,!long_max)


let trie_longueur (tableau : string array) (min_long : int) (max_long : int) : string array array =   
  assert((min_long >=0) && (min_long<=max_long) && (Array.length tableau <> 0));
  let tab_tri = Array.make ((max_long - min_long) + 1) [||] in      (*Initialisation du tableau avec des sous-tableaux (qui comporteront les mots de longueur allant de min_int à max_int).*)

  for i = min_long to max_long do                          (*Pour chacun de ces tableaux.*)
    let nb_de_longueur_voulue = ref 0 in

    for k = 0 to Array.length tableau - 1 do
      if String.length tableau.(k) = i then
        nb_de_longueur_voulue := 1 + !nb_de_longueur_voulue;    (*On compte le nombre de mots ayant i lettres.*)
    done;

    let tab_tmp = Array.make !nb_de_longueur_voulue "" in       (*On peut alors initialiser notre sous-tableau avec le nombre de mots qu'on sait devoir ajouter.*)

    let increment = ref 0 in                    (*Indice du sous-tableau.*)
    for j = 0 to Array.length tableau - 1 do
      if String.length tableau.(j) = i then         (*On a croisé un mot de la bonne longueur.*)
        begin
        tab_tmp.(!increment) <- tableau.(j);        (*On l'ajoute au sous-tableau et on peut incrémenter l'indice de ce sous-tableau.*)
        incr increment;
        end;
    done;

    tab_tri.(i - min_long) <- tab_tmp;          (*Le sous-tableau est ajouté au grand tableau.*)
  done;
  tab_tri

let tri_alphabetique (tableau : string array array) : unit =  (*On réalise un tri par sélection.*)
  let longueur = Array.length tableau in
  for i = 0 to longueur - 1 do                   (*Pour chaque sous-tableau.*)
    for j=0 to Array.length(tableau.(i))-1 do
      let temp_min = ref tableau.(i).(j) in         (*Initialisation des informations sur le minimum.*)
      let temp_indice = ref j in
      for k=(j+1) to Array.length(tableau.(i))-1 do 
        if !temp_min > tableau.(i).(k) then         (*On détermine le minimum de ce qui n'a pas encore été trié.*)
          begin
          temp_min := tableau.(i).(k);      
          temp_indice := k;
          end
      done;
      tableau.(i).(!temp_indice) <- tableau.(i).(j);      (*On échange alors l'élément le plus petit avec le premier élément pas encore trié.*)
      tableau.(i).(j) <- !temp_min;
    done;
  done


let creer_string_tab (tableau : string array array) : string = 
  let str = ref "[| " in
  for i = 0 to Array.length tableau -1 do     (*Pour chaque sous-tableau.*)
    begin
    str := !str ^ "[| "; 
    for j = 0 to Array.length tableau.(i) -1 do   (*Pour chaque élément dans ce sous-tableau.*)
      begin
      str := !str ^ "\u{22}" ^ (tableau.(i).(j)) ^ "\u{22}";  (*On ajoute à la chaîne de caractères l'élément encadré de guillemets : "" (le caractère ASCII : \u{22}).*)
      if j <> Array.length tableau.(i) -1 then         (*Si ce n'est pas le dernier élément du sous-tableau, il y aura un suivant, donc on ajoute ";" à la chaîne de caractères.*)
        str := !str ^ " ; ";
      end;
    done;
    str := !str ^ " |]";        (*Puis on complète en "fermant le tableau".*)
    end;
    if i <> Array.length tableau - 1 then
      str := !str ^ " ; ";
  done;
  str := !str ^ " |]";
  !str               

let creer_tableau_fichier_txt (nom_fichier : string) (tableau : string array) (min_long : int) (max_long : int): unit =
  assert((min_long >=0) && (min_long<=max_long));
  assert(not(String.contains nom_fichier '.'));                      (*L'extension sera ajoutée par la suite, on ne veut pas que l'utilisateur la choisisse.*)
  let tab_trie_long = trie_longueur tableau min_long max_long in    (*On trie le tableau par taille.*)
  tri_alphabetique tab_trie_long;                                           (*Puis par ordre alphabétique.*)
  let string_tab = creer_string_tab tab_trie_long in                        (*On le transforme en chaîne de caractère (pour pouvoir le renvoyer dans un fichier txt).*)
  let x = open_out (nom_fichier ^ ".txt") in                                (*On crée le fichier .txt .*)
  begin
  output_string x string_tab;                                               (*Et on y ajoute la liste créée.*)
  close_out x;
  end;;

let noms_absents (grosse_base : string array) (petite_base : string array) (min_long : int) (max_long : int): unit = 
  assert((min_long >=0) && (min_long<=max_long) && (Array.length grosse_base <> 0) && (Array.length petite_base <> 0));     (*La fonction trie_longueur a besoin de tableaux non vide.*)
  let gB = trie_longueur grosse_base min_long max_long in     (*On commence par trier les deux bases rentrées pour optimiser la recherche.*)
  tri_alphabetique gB;
  let pB = trie_longueur petite_base min_long max_long in
  tri_alphabetique pB;
  for nb = 0 to max_long - min_long do                            (*On obtient alors des tableaux de tableaux. On se place dans chacun de ces sous-tableaux (comportant donc des mots de la même longueur).*)
    for i = 0 to Array.length pB.(nb) - 1 do       (*Pour chaque mot de la petite base.*)
      let verif = ref 0 in
        for j = 0 to Array.length gB.(nb) - 1 do         (*On le compare à chaque mot de la grande base.*)
          if pB.(nb).(i) = gB.(nb).(j) then 
            begin
            verif := 1;                                             (*Si on l'a trouvé, on peut s'arrêter là, il se trouve bien dans la grande base.*)
            end
        done;
      if !verif = 0 then                                            (*Si on ne l'a pas trouvé (verif vaut toujours 0), on peut alors l'afficher, il est manquant dans la grande base.*)
        begin
        print_string pB.(nb).(i);
        print_newline();
        end
    done;
  done;;