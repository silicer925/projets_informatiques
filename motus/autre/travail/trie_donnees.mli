(* Fichier d'interface de trie_donnees.ml *)

(** fonction longueur_extremum
    - description : retourne les longueurs minimale et maximale des mots contenus dans un tableau.
	- entrée : string array
	- précondition : liste non vide
	- sortie : int * int (nombre de caractères minimal des éléments du tableau ;  nombre de caractères maximal des éléments du tableau)
	- postcondition : aucun effet de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val longueur_extremum : string array -> int*int


(** creer_tableau_fichier_txt
    - description : trie un tableau en sous-tableaux de longueur de mots (pour des longueurs de mot données) et par ordre alphabétique. Renvoie le tout dans un fichier .txt dont l'utilisateur peut donner le nom qu'il souhaite.
	- entrée : string (nom du fichier .txt à créer), string array (tableau à trier), int (longueur minimale du nombre de lettres du tableau que l'on veut garder), int (idem avec la longueur maximale)
	- précondition : entiers positifs, l'entier de longueur maximale supérieur ou égal à celui de longueur minimale et le nom du fichier de retour sans extension (en particulier, sans .txt)
	- sortie : unit (créé un fichier .txt)
	- postcondition : aucun effet de bord, créé un fichier .txt dans le répertoire courant
    - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val creer_tableau_fichier_txt : string -> string array -> int -> int -> unit


(** fonction noms_absents
    - description : affiche les mots compris dans un second tableau, mais pas dans le premier
	- entrée : string array (tableau dans lequel on va chercher), string array (tableau dont on veut comparer les mots), int (longueur minimale du nombre de lettres des mots des tableaux que l'on veut comparer), int (idem avec la longueur maximale)
	- précondition : liste non vide, entiers positifs et l'entier de longueur maximale supérieur ou égal à celui de longueur minimale
	- sortie : unit (affiche les mots absents dans le premier tableau)
	- postcondition : aucun effet de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val noms_absents : string array -> string array -> int -> int -> unit