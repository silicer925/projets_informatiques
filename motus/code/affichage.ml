(*fonction d'affichage qui affiche un mot dans le tableau de jeu motus en fonction de l'état de ses lettres*)
(*exemple : | c | h | a | t | 
            | * | ° |   | ° |  *)
let afficher_mot (mot : (string * (Utilitaire.etat array))) (nb_lettres : int) = 
  assert ((nb_lettres >= 0) && (String.length (fst mot)) = nb_lettres && (Array.length (snd mot)) = nb_lettres) ;
  let chaine, etats = mot in (*on récupère respectivement le mot en lui même et l'état de ses lettres*)
  for cpt = 0 to (nb_lettres-1) do
    
    print_string "| " ;
    print_string (String.sub chaine cpt 1) ;
    print_string " "
  done;
  print_endline "|";

  (*affichage de l'état de ses lettres*)
  (*NB : ° -- lettre contenue dans le mot mais mal placée
         * -- lettre bien placée et a fortiori contenue dans le mot 
           -- lettre non contenue dansdans le mot mystère (ou alors contenue mais déjà bien placée ou mal placée) *)
  for cpt = 0 to (nb_lettres-1) do 
    print_string "| " ;
    if (etats.(cpt) = Utilitaire.Bof)
    then print_string "°"
    else if (etats.(cpt) = Utilitaire.Bonne)
    then print_string "*"
    else print_string " " ;
    print_string " "
  done;
  print_endline "|"


(*fonction auxiliaire qui crée une ligne d'affichage (en string) de motus vide dans le but d'améliorer la complexité
exemple : "|   |   |   |" *)
let creer_ligne_vide (nb_lettres : int) : string =
  assert (nb_lettres >= 0);
(*une telle chaine est en réalité une juxtaposition de blocs "|  " et d'un "|" final*)
  let brique = "|   " and chaine = ref "|" in
  for cpt = 0 to (nb_lettres -1) do
    chaine := brique ^ !chaine
  done;
  !chaine
  

(*affichage global de la grille en fonction du nombre d'essai, de la taille des mots et du nombres de mots déjà proposés*)
let afficher_grille (nb_lettres : int) (nb_essai_restant : int) (res : (string * Utilitaire.etat array) array): unit =
  if (res <> ([| |]) ) then
    assert ((String.length (fst(res.(0))) = nb_lettres) && (Array.length (snd(res.(0))) = nb_lettres));
(*on crée une ligne simple dans le but d'améliorer la complexité ex : "------------"  *)
  let ligne = String.make (nb_lettres * 4 + 2) '-' in
  (*on crée une ligne vide ex : "|   |   |   |   |" *)
  let ligne_vide = creer_ligne_vide nb_lettres in 
  let nb_mots = Array.length res in
  print_endline "MO MO MO MOTUS !!!!";
  print_newline (); 
  (*disjonction de cas en fonction de la vacuité du tableau de mots déjà proposés*)
  if (nb_mots = 0)
  then
    begin
    (*si le tableau est vide, alors la grille n'est qu'une succession de ligne et de ligne_vide*)
      for cpt = 0 to (nb_essai_restant-1) do
        print_endline ligne_vide;
        print_endline ligne_vide;
        print_endline ligne; 
      done;
    end
  else
    begin
    (*si le tableau n'est pas vide on procède ne deux temps : 
    on commence par afficher les mots avec la fonction définie plus haut
    puis on ajoute autant de lignes vides qu'il ne reste d'essais au joueur*)
      for cpt = 0 to (nb_mots -1) do 
        let mot = res.(cpt) in
        afficher_mot mot nb_lettres; 
        print_endline ligne;
        
      done; 
      for cpt = 0 to (nb_essai_restant-1) do 
        print_endline ligne_vide;
        print_endline ligne_vide;
        print_endline ligne;
        
      done;
    end 

let logo () : unit =
  let rand_nomb = Random.int 18 in
  let logo_1 = [|"
          .         .
         ,8.       ,8.              ,o888888o.     8888888 8888888888 8 8888      88    d888888o.
        ,888.     ,888.          . 8888     `88.         8 8888       8 8888      88  .`8888:' `88.
       .`8888.   .`8888.        ,8 8888       `8b        8 8888       8 8888      88  8.`8888.   Y8
      ,8.`8888. ,8.`8888.       88 8888        `8b       8 8888       8 8888      88  `8.`8888.
     ,8'8.`8888,8^8.`8888.      88 8888         88       8 8888       8 8888      88   `8.`8888.
    ,8' `8.`8888' `8.`8888.     88 8888         88       8 8888       8 8888      88    `8.`8888.
   ,8'   `8.`88'   `8.`8888.    88 8888        ,8P       8 8888       8 8888      88     `8.`8888.
  ,8'     `8.`'     `8.`8888.   `8 8888       ,8P        8 8888       ` 8888     ,8P 8b   `8.`8888.
 ,8'       `8        `8.`8888.   ` 8888     ,88'         8 8888         8888   ,d8P  `8b.  ;8.`8888
,8'         `         `8.`8888.     `8888888P'           8 8888          `Y88888P'    `Y8888P ,88P'
" ; 
"                :::   :::       ::::::::   :::::::::::      :::    :::       ::::::::
                :+:+: :+:+:     :+:    :+:      :+:          :+:    :+:      :+:    :+:
               +:+ +:+:+ +:+    +:+    +:+      +:+          +:+    +:+      +:+
               +#+  +:+  +#+    +#+    +:+      +#+          +#+    +:+      +#++:++#++
               +#+       +#+    +#+    +#+      +#+          +#+    +#+             +#+
               #+#       #+#    #+#    #+#      #+#          #+#    #+#      #+#    #+#
               ###       ###     ########       ###           ########        ######## 
";"
                   .S_SsS_S.     sSSs_sSSs    sdSS_SSSSSSbs   .S       S.     sSSs
                  .SS~S*S~SS.   d%%SP~YS%%b   YSSS~S%SSSSSP  .SS       SS.   d%%SP
                  S%S `Y' S%S  d%S'     `S%b       S%S       S%S       S%S  d%S'
                  S%S     S%S  S%S       S%S       S%S       S%S       S%S  S%|
                  S%S     S%S  S&S       S&S       S&S       S&S       S&S  S&S
                  S&S     S&S  S&S       S&S       S&S       S&S       S&S  Y&Ss
                  S&S     S&S  S&S       S&S       S&S       S&S       S&S  `S&&S
                  S&S     S&S  S&S       S&S       S&S       S&S       S&S    `S*S
                  S*S     S*S  S*b       d*S       S*S       S*b       d*S     l*S
                  S*S     S*S  S*S.     .S*S       S*S       S*S.     .S*S    .S*P
                  S*S     S*S   SSSbs_sdSSS        S*S        SSSbs_sdSSS   sSS*S
                  SSS     S*S    YSSP~YSSY         S*S         YSSP~YSSY    YSS'
                          SP                       SP
                          Y                        Y
" ; "
                      '##::::'##::'#######::'########:'##::::'##::'######::
                       ###::'###:'##.... ##:... ##..:: ##:::: ##:'##... ##:
                       ####'####: ##:::: ##:::: ##:::: ##:::: ##: ##:::..::
                       ## ### ##: ##:::: ##:::: ##:::: ##:::: ##:. ######::
                       ##. #: ##: ##:::: ##:::: ##:::: ##:::: ##::..... ##:
                       ##:.:: ##: ##:::: ##:::: ##:::: ##:::: ##:'##::: ##:
                       ##:::: ##:. #######::::: ##::::. #######::. ######::
                      ..:::::..:::.......::::::..::::::.......::::......:::
" ; "
                                     🅜    🅞    🅣    🅤    🅢
" ; "
                                     🅼    🅾    🆃    🆄    🆂
" ; "
                                                  _|
                      _|_|_|  _|_|      _|_|    _|_|_|_|  _|    _|    _|_|_|
                      _|    _|    _|  _|    _|    _|      _|    _|  _|_|
                      _|    _|    _|  _|    _|    _|      _|    _|      _|_|
                      _|    _|    _|    _|_|        _|_|    _|_|_|  _|_|_|
" ; "
                                                ##
                                                ##
                            ### ##    #####   ######   ##   ##   #####
                            ## # ##  ##   ##    ##     ##   ##  ##
                            ## # ##  ##   ##    ##     ##   ##   ####
                            ## # ##  ##   ##    ##     ##  ###      ##
                            ##   ##   #####      ###    ### ##  #####
" ; "
                                                 888
                                                 888
                                                 888 
                          88888b.d88b.   .d88b.  888888 888  888 .d8888b
                          888  888  88b d88  88b 888    888  888 88K
                          888  888  888 888  888 888    888  888  Y8888b.
                          888  888  888 Y88..88P Y88b.  Y88b 888      X88
                          888  888  888   Y88P     Y888   Y88888  88888P'
" ; "
                              _______  _____  _______ _     _ _______
                              |  |  | |     |    |    |     | |______
                              |  |  | |_____|    |    |_____| ______|
" ; "
                                               :
                                              t#,               :                .
                                             ;##W.              Ef              ;W
                               ..       :   :#L:WE    GEEEEEEEL E#t            f#E
                              ,W,     .Et  .KG  ,#D   ,;;L#K;;. E#t          .E#f
                             t##,    ,W#t  EE    ;#f     t#E    E#t         iWW;
                            L###,   j###t f#.     t#i    t#E    E#t fi     L##Lffi
                          .E#j##,  G#fE#t :#G     GK     t#E    E#t L#j   tLLG##L
                         ;WW; ##,:K#i E#t  ;#L   LW.     t#E    E#t L#L     ,W#i
                        j#E.  ##f#W,  E#t   t#f f#:      t#E    E#tf#E:    j#E.
                      .D#L    ###K:   E#t    f#D#;       t#E    E###f    .D#j
                     :K#t     ##D.    E#t     G#t        t#E    E#K,    ,WK,
                     ...      #G      ..       t          fE    EL      EG.
                              j                            :    :       ,
" ; "

                                                    tttt
                                                 ttt:::t
                                                 t:::::t
                                                 t:::::t
     mmmmmmm    mmmmmmm      ooooooooooo   ttttttt:::::ttttttt    uuuuuu    uuuuuu      ssssssssss
   mm:::::::m  m:::::::mm  oo:::::::::::oo t:::::::::::::::::t    u::::u    u::::u    ss::::::::::s
  m::::::::::mm::::::::::mo:::::::::::::::ot:::::::::::::::::t    u::::u    u::::u  ss:::::::::::::s
  m::::::::::::::::::::::mo:::::ooooo:::::otttttt:::::::tttttt    u::::u    u::::u  s::::::ssss:::::s
  m:::::mmm::::::mmm:::::mo::::o     o::::o      t:::::t          u::::u    u::::u   s:::::s  ssssss
  m::::m   m::::m   m::::mo::::o     o::::o      t:::::t          u::::u    u::::u     s::::::s
  m::::m   m::::m   m::::mo::::o     o::::o      t:::::t          u::::u    u::::u        s::::::s
  m::::m   m::::m   m::::mo::::o     o::::o      t:::::t    ttttttu:::::uuuu:::::u  ssssss   s:::::s
  m::::m   m::::m   m::::mo:::::ooooo:::::o      t::::::tttt:::::tu:::::::::::::::uus:::::ssss::::::s
  m::::m   m::::m   m::::mo:::::::::::::::o      tt::::::::::::::t u:::::::::::::::us::::::::::::::s
  m::::m   m::::m   m::::m oo:::::::::::oo         tt:::::::::::tt  uu::::::::uu:::u s:::::::::::ss
  mmmmmm   mmmmmm   mmmmmm   ooooooooooo             ttttttttttt      uuuuuuuu  uuuu  sssssssssss
" ; "
                                                _
                                               (_)
          _  _   _  _         _  _  _        _ (_) _  _       _         _         _  _  _  _
         (_)(_)_(_)(_)     _ (_)(_)(_) _    (_)(_)(_)(_)     (_)       (_)      _(_)(_)(_)(_)
        (_)   (_)   (_)   (_)         (_)      (_)           (_)       (_)     (_)_  _  _  _
        (_)   (_)   (_)   (_)         (_)      (_)     _     (_)       (_)       (_)(_)(_)(_)_
        (_)   (_)   (_)   (_) _  _  _ (_)      (_)_  _(_)    (_)_  _  _(_)_       _  _  _  _(_)
        (_)   (_)   (_)      (_)(_)(_)           (_)(_)        (_)(_)(_) (_)     (_)(_)(_)(_)
" ; "
                                                    /
                        ___  __    __     _____    /M     ___   ___   ____
                        `MM 6MMb  6MMb   6MMMMMb  /MMMMM  `MM    MM  6MMMMb
                         MM69 `MM69 `Mb 6M'   `Mb  MM      MM    MM MM'    `
                         MM'   MM'   MM MM     MM  MM      MM    MM YM.
                         MM    MM    MM MM     MM  MM      MM    MM  YMMMMb
                         MM    MM    MM MM     MM  MM      MM    MM      `Mb
                         MM    MM    MM YM.   ,M9  YM.  ,  YM.   MM L    ,MM
                        _MM_  _MM_  _MM_ YMMMMM9    YMMM9   YMMM9MM_MYMMMM9
" ; "
                                                     I8
                                                     I8
                                                  88888888
                                                     I8
                    ,ggg,,ggg,,ggg,     ,ggggg,      I8    gg      gg    ,g,
                   ,8' '8P' '8P   8,   dP    Y8ggg   I8    I8      8I   ,8'8,
                   I8   8I   8I   8I  i8'    ,8I    ,I8,   I8,    ,8I  ,8'  Yb
                  ,dP   8I   8I   Yb,,d8,   ,d8'   ,d88b, ,d8b,  ,d8b,,8'_   8)
                  8P'   8I   8I   `Y8P''Y8888P''    88P'''Y888P''Y88P'`Y8P' 'YY8P8P
" ; "
                        @@@@@@@@@@    @@@@@@   @@@@@@@  @@@  @@@   @@@@@@
                        @@@@@@@@@@@  @@@@@@@@  @@@@@@@  @@@  @@@  @@@@@@@
                        @@! @@! @@!  @@!  @@@    @@!    @@!  @@@  !@@
                        !@! !@! !@!  !@!  @!@    !@!    !@!  @!@  !@!
                        @!! !!@ @!@  @!@  !@!    @!!    @!@  !@!  !!@@!!
                        !@!   ! !@!  !@!  !!!    !!!    !@!  !!!   !!@!!!
                        !!:     !!:  !!:  !!!    !!:    !!:  !!!       !:!
                        :!:     :!:  :!:  !:!    :!:    :!:  !:!      !:!
                        :::     ::   ::::: ::     ::    ::::: ::  :::: ::
                         :      :     : :  :      :      : :  :   :: : :
 " ; "
                             _         _         _         _         _
                           _( )__    _( )__    _( )__    _( )__    _( )__
                         _|     _| _|     _| _|     _| _|     _| _|     _|
                        (_ M _ (_ (_ O _ (_ (_ T _ (_ (_ U _ (_ (_ S _ (_
                          |_( )__|  |_( )__|  |_( )__|  |_( )__|  |_( )__|
" ; "
                                =====================================
                                ===================  ================
                                =  =  = ====   ===    ==  =  ===   ==
                                =        ==     ===  ===  =  ==  =  =
                                =  =  =  ==  =  ===  ===  =  ===  ===
                                =  =  =  ==  =  ===  ===  =  ====  ==
                                =  =  =  ==  =  ===  ===  =  ==  =  =
                                =  =  =  ===   ====   ===    ===   ==
                                ====================================="
|] in
  print_newline ();
  print_string logo_1.(rand_nomb);
  print_newline ()
