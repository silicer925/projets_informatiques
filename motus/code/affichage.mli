(** fonction afficher_mot
	- description : affiche deux lignes de tableau dans le terminal pour afficher un mot du motus
	- entrée : un tableau de mot et de leurs états : un tableau de tuples :string * (Utilitaire.etat array), le nombre de lettres des mots : int
	- précondition : les mots contenus dans le premier élément de chaque tuple sont de longueur du second paramètre
	- sortie : rien
	- postcondition : ont été affichées 2 lignes en sortie utilisateur : 
      une première ligne avec le mot dans un format de grille (| t | a | h |)
      une seconde ligne avec les états de chaque lettre (symboles * et °) : (| * | ° |   |) 
	- lève une Assert_failure si les préconditions ne sont pas respectées
	
*)
val afficher_mot : string * (Utilitaire.etat array) -> int -> unit


(** fonction creer_ligne_vide
	- descritpion : crée une variable string d'une ligne pour améliorer la complexité de l'affichage du tableau
	- entrée : le nombre de lettre : int
	- précondition : le nombre est un entier naturel
	- sortie : une chaine de caratère : string
	- postcondition : la chaine de caractère renvoyée comporte *nombre de lettres* fois la chaine "|  ", avec à la fin de la chaine l'ajout de "|"
	- lève une Assert_failure si les préconditions ne sont pas respectées
	*)
val creer_ligne_vide : int -> string

(** fonction afficher_grille
	- description : affiche une grille de motus à un instant t
	- entrée : le nombre de lettres : int, le nombre d'essais restant à l'utilisateur : int, un tableau des mots à afficher ainsi que leurs états(ie les symboles * et ° à afficher) : (string * etat array) * array
	- précondition :les deux paramêtres sont des entiers naturels, et, si le tableau est non vide, les composants de chaques tuples doivent tous être de la taille du nombre passé en paramêtre
	- sortie : aucune
	- postcondition : une grille a été affichée avec comme premières lignes des lignes comportant des mots
      les autres lignes sont des lignes de grille vides, il y en a  *nombre d'essais restant*
	- lève une Assert_failure si les préconditions ne sont pas respectées
*)

val afficher_grille : int -> int -> (string * Utilitaire.etat array) array -> unit

(** fonction logo
  - description : affiche un logo aléatoire de motus
	- entrée : aucune
	- précondition : aucune
	- sortie : aucune (affichage d'un logo)
	- postcondition : aucune
	*)
	val logo : unit -> unit
