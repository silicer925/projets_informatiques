let jeu () = 
  let partie = Sys.argv.(1) in (*on récupère le type de partie voulu par l'utilisateur*)
  assert (partie = "JOUEUR" || partie = "joueur" || partie = "ORDI_HASARD" || partie = "ordi_hasard" || partie = "ORDI_INTELLIGENT" || partie = "ordi_intelligent"); (*On s'assure que l'utilisateur a bien rentré un nom de partie valide*)
  if partie = "JOUEUR" || partie = "joueur" then (*si l'utilisateur veut jouer une partie, on lance (depuis le fichier joueur.ml) une partie pour joueur*)
    begin Joueur.debut Utilitaire.lexique Utilitaire.dico_choix end
  else if partie = "ORDI_HASARD" || partie = "ordi_hasard" then (*si l'utilisateur regarder l'ordi jouant au hasard jouer, on lance (depuis le fichier ordi_hasard.ml) une partie pour l'ordi*)
    begin Ordi_hasard.partie_ordi_hasard Utilitaire.lexique end
  else (*si l'utilisateur regarder l'ordi jouant intelligement jouer, on lance (depuis le fichier ordi_intelligent.ml) une partie pour l'ordi*)
    begin Ordi_intelligent.partie_ordi_intelligent Utilitaire.lexique end

let _ = jeu ()
