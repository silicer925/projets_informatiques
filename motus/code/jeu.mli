(** fonction jeu (fonction permettant de lancer une partie)
	- entrée : aucune
	- précondition : lors de l'exécution, les arguments doivent être : "joueur", "ordi_hasard" ou "ordi_intelligent", en minuscule ou majuscule
	- sortie : unit 
	- postcondition : lance une partie selon les demandes de l'utilisateur, pas d'effets de bord
    - lève Assert_failure si les préconditions ne sont pas respectées
*)
val jeu : unit -> unit

