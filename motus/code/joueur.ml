Random.self_init()
exception Trouve 
exception Break

(*fonction de rehcerche dichotomique dans un tableau pour avoir une complexité en log2(n)*)
let dichotomie (tab : 'a array) (x : 'a) : bool=
  try
    let gauche, droite = ref 0, ref (Array.length tab) in
    while !gauche < !droite do
      let milieu = (!gauche + !droite) / 2 in
      if tab.(milieu) = x 
        then raise Trouve ; (*on a trouvé l'élément recherché, on lève une exception pour interrompre la recherche*)
      if tab.(milieu) > x (*cas où l'élément recherché est "à gauche" de l'élément central*)
        then droite := milieu 
      else gauche := milieu+1(*cas où l'élément recherché est "à droite" de l'élément central*)
    done ;
    false
  with Trouve ->true

(*Fonction d'affichage informant le joueur de sa victoire : il a trouvé le mot mystère*)
let gagner (mot_a_trouver : string) (nb_essais_restants : int) : unit =
  assert (nb_essais_restants >= -1 );
  print_endline "Saperlipopette ! Tu as gagné !!";
  print_string "Bravoooooo tu as trouvé le nombre mystère qui était " ;
  print_string mot_a_trouver;
  print_endline " !!!!!!";
  print_string "Et en plus, il te restait encore ";
  print_int (nb_essais_restants +1);
  print_endline " essai(s) !!!" 

(*Fonction d'affichage pour informer le joueur qu'il a perdu : il n'a pas trouvé le mot mystère malgrés les essais dont il disposait*)
let perdu (mot_a_trouver : string) : unit = 
  print_endline "Mouhahahahahaha tu as perdu !!!";
  print_endline "Je savais bien que j'étais trop fort pour toi....." ;
  print_endline "Dans mon infinie bonté, je vais te révéler le mot mystère...";
  print_string "C'était.... " ;
  print_endline mot_a_trouver;
  print_endline "Pas trop déçu ? Avec quelques essais en plus, tu aurais peut-être réussi..."



(*fonction de formatage de mot : on regarde s'il y a d'autres caractères que des lettres sans accents*)
(*on permet à l'utilisateur de rentrer des majuscules ou des minuscules selon son bon vouloir*)
let formate_mot (mot : string) : string = 
  (*passage du mot en majuscules*)
  let mot_bis = String.uppercase_ascii mot in
  let alpha = [|'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'; 'I'; 'J';'K';'L';'M';'N';'O';'P';'Q';'R';'S';'T';'U';'V';'W';'X';'Y';'Z'|] in
  try
    for cpt = 0 to (String.length mot -1) do 
    (*on regarde si chaque lettre fait partie des lettres autorisées*)
      if not(dichotomie alpha mot_bis.[cpt] ) 
      (*si ce n'est pas le cas on lève une exception qui va renvoyer un mot spécial de longueur 2 (qui ne peut donc pas être proposé par l'utilisateur car le mot_secret peut faire de 3 à 7 lettres*)
      then raise Break;
    done;
    mot_bis
  with Break -> "zz"

(*easter egg voulu par un des membre du groupe (je vous laisse deviner lequel!) qui renvoie une petite surprise!*)
let easter_egg (mot : string) : unit = 
  if mot = "DUNE"
    then 
      begin 
      print_endline "Oh mais incroyable, ferais tu partie de cette caste d'initiés qui connaissent cet incroyable, que dis-je divin, film !!!!";
      print_endline "VIVE DUNE !!!!!!!";
      print_endline "Cela vaut bien une petite victoire en plus sapristi !!!!"
      end
  

(*fonction de jeu proprement dite prenant en paramètre le nombre d'essais voulu par l'utilisateur*)
let jouer (nb_essais : int) (dictionnaire : string array array) (petite_base : string array array): unit =
  assert (not((Array.length dictionnaire) = 0) && not((Array.length petite_base) = 0));
(*on tire aléatoirement la longueur du mot_secret*)
  let lg = (Random.int 4) +3  in
  (*on tire aléatoirement un mot parmi les mots de longeur choisie plus haut*)
  let mot_a_trouver = petite_base.(lg-3).(Random.int (Array.length petite_base.(lg-3))) in
  (*variable qui va prendre en paramètre successivment les mots proposés pas le joueur*)
  let mot_proposer = ref "" in
  (*variable qui va contenir peu à peu tous les mots  proposés par le joueur ainsi que leurs états respectifs*)
  let tab_etats = ref [| |] in
  Affichage.afficher_grille lg nb_essais ([| |]);
  print_string "Le mot que tu dois découvrir est en ";
  print_int lg;
  print_endline " lettres";
  let cpt = ref 1 in 
  let cpt_garanti_terminaison = ref (5 * nb_essais) in
  (*on continue à demander des mots à l'utilisateur tant qu'il a encore des essais, qu'il n'a trouvé ni l'easter egg ni le mot_mystere*)
  while nb_essais >= !cpt && (!mot_proposer <> mot_a_trouver) && (!mot_proposer <> "DUNE") && (!cpt_garanti_terminaison >0 ) do
    cpt_garanti_terminaison := !cpt_garanti_terminaison - 1;
    print_endline "Nb : ° - lettre contenue dans le mot mystère mais mal placée, * - lettre bien placée ";
    print_endline "Quel mot me proposes tu ?"; 
    mot_proposer := read_line() ;
    print_newline ();
    print_newline ();
    mot_proposer := (formate_mot !mot_proposer);
    (*on vérifie que le mot proposé est de la bonne longueur*)
    if (((String.length !mot_proposer) <> lg) && (not(!mot_proposer = "DUNE")))
    then 
      begin 
        print_endline "Le mot que tu as passé en paramètre n'est pas de la bonne taille !";
        
      end
    (*on vérifie ensuite que le mot proposé ne comporte pas de caractères spéciaux *)
    else if !mot_proposer = "zz"
    then print_endline "Ce mot comporte des caractères spéciaux interdits !!"
    

    (*si les deux conditions ci-dessus sont respectées, on regarde si le mot existe (ie s'il est sontenu dans la base de donnée)*)
    else if (not( dichotomie  dictionnaire.(lg-3) !mot_proposer) && (not(!mot_proposer = "DUNE")))
      then print_endline "Ce mot n'existe pas !"
    else if (not(!mot_proposer = "DUNE"))
      then
        (*si toutes les conditions ci-dessus sont respectées, on regarde l'état de chaque lettre*)
        let etat = (!mot_proposer , Utilitaire.verification !mot_proposer mot_a_trouver lg) in
        begin 
          tab_etats := Array.append !tab_etats [|etat|];
          Affichage.afficher_grille lg (nb_essais- (!cpt)) !tab_etats;
          cpt := !cpt +1;
        end
  done; 
  (*si le joueur a trouvé l'easter egg*)
  if (!cpt_garanti_terminaison = 0)
    then print_endline "Trop de mots inconnus, veuillez arrêter de rentrer des mots inexistants ou non conformes au type demandé"
    else if (!mot_proposer = "DUNE")
      then easter_egg "DUNE"
  (*cas où l'utilisateur a soit trouvé le mot_secret, dans ce cas c'est gagné, sinon c'est qu'il a épuisé toutes ses tentatives, il a perdu *)
      else if (!mot_proposer <> mot_a_trouver)
        then 
          begin perdu mot_a_trouver end
        else begin gagner mot_a_trouver (nb_essais - !cpt) end 

(*fonction d'affichage d'accueil du joueur qui lui permet de choisir le nombre d'essais qu'il veut avoir *)
let debut (dictionnaire : string array array)  (petite_base : string array array) = 
  Random.self_init();
  assert (not((Array.length dictionnaire) = 0) && not((Array.length petite_base) = 0));
  Affichage.logo();
  print_endline "Bonjour cher joueur !!";
  print_endline "Tu vas pouvoir tenter, de découvrir le mot secret... ";
  print_endline "Note de jeu : les mots doivent être entrés formatés (ie sans accents en tout genre)";
  print_endline "Dis moi, combien d'essais veux-tu avoir ? ";
  print_endline "Tu peux en avoir entre 3 si tu te sens tel M. Sihrener...";
  print_endline "... Jusqu'à 15 si la sureté de M. Doreau te correspond plus";
  print_endline "Alors, quel est ton choix ?" ;
  print_endline "Entre un chiffre entre 3 et 15 pour choisir ton nombre d'essai : ";
  

  let nb_essais = ref (read_int ()) in
  assert ( !nb_essais <= 15 && 3 <= !nb_essais);
  print_endline "Que la partie commenceeeee !";
  
  (*l'utilisateur peut rejouer après chaque partie, et peut même changer de nombre de tentative si l'envie le prend!*)
  jouer !nb_essais dictionnaire petite_base ;
  print_endline "Veux tu encore jouer ? (tape OUI ou NON) : ";
  let choix = ref (formate_mot((read_line ()))) in
  if (!choix = "OUI")
    then begin
    print_endline "Combien veux-tu d'essais cette fois-ci ? (tape un nombre entre 3 et 15) : ";
    nb_essais := (read_int ()) ;
    end;
  while formate_mot(!choix) = "OUI" do
    begin jouer !nb_essais dictionnaire petite_base;
    print_endline "Veux tu encore jouer ? (tape OUI ou NON) : ";
    choix := (read_line ());
    if (!choix = "OUI")
    then  begin 
            print_endline "Combien veux-tu d'essais cette fois-ci ? (tape un nombre entre 3 et 15) : ";
            nb_essais := (read_int ()) ;
          end;

    end
  done

