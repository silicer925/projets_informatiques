(** fonction dichotomie
	- description : fait une recherche dichotomique dans un tableau trié
	- entrée : tableau trié : 'a array, élément cherché : 'a 
	- précondition : l'élement cherché et les élements du tableau sont de même type, le tableau est trié dans l'ordre croissant
	- sortie : Booléen
	- postcondition : Le booléen correspond à la présence ou non de l'élément recherché dans le tableau passé en paramètre

NB : la précondition d'avoir un tableau rangé dans l'odre croissant est assuré dans notre algorithme, et vérifier cette précondition nous ferait perdre tout l'avantage d'une recherche dichotomique en nous ramenant à une recherche en O(n)
*)

val dichotomie : 'a array -> 'a -> bool

(** fonction gagner
	- description : affichage pour le joueur quand il gagne
	- entrée : le mot mystère : string, le nombre d'essais restant à l'utilisateur : int
	- précondition : le nombre d'essais est un nombre entier naturel
	- sortie : aucune
	- postcondition : les messages : "Saperlipopette! Tu as gagné!!"
                                  "Bravoooooo tu as trouvé le nombre mystère qui était ...*1 !!"
                                  "Et en plus, il te restait encore ...*2 essais"
                                  avec *1 = mot_mystère passé en paramètre, et *2 le nombre d'essais restant passé en paramètre
	- lève une Assert_failure si les préconditions ne sont pas respectées
*)
val gagner : string -> int -> unit

(** fonction perdu
	- description : affichage pour le joueur quand il perd
	- entrée : mot à trouver : string
	- précondition : aucune
	- sortie : aucune
	- postcondition : dans la sortie utilisateur ont été affichés : 
	"Mouhahahahahaha tu as perdu !!!"
	"Je savais bien que j'étais trop fort pour toi....."
	"Dans mon infinie bonté, je vais te révéler le mot mystère..."
	"C'était .... " *1
	"Pas trop déçu? Avec quelques essais en plus, tu aurais peut-être réussi..."
	avec *1 : le mot mystère que le joueur n'a pas trouvé
*)
val perdu : string -> unit

(** fonction formate_mot
	- fonction de formatage de mot entré par l'utilisateur
	- entrée : mot : string
	- précondition : aucune
	- sortie : un autre mot : string
	- postcondition : le mot renvoyé est soit "zz" s'il y a des caractères spéciaux dans le mot passé en paramètre
	sinon, le mot renvoyé est le mot passé en paramêtre en lettres majuscules
*)
val formate_mot : string -> string

(** fonction easter_egg
	- description : petite surprise!!
	- entrée : mot : string
	- précondition : aucune
	- sortie : aucun
	- postcondition : si le mot est dune, afficher sur la sortie utilisateur : 
	"oh mais incroyable, ferais tu partie de cette caste d'initiés qui connaissent cet incroyable, que dis-je divin, film!!!!"
	"VIVE DUNE !!!!!!!"
	"Cela vaut bien une petite victoire en plus sapristi !!!!;"
	sinon ne fait rien
*)
val easter_egg : string -> unit

(** fonction jouer
	- description : fait jouer l'utilisateur
	- entrée : le nombre d'essais du joueur pour découvrir le mot : int, le dictionnaire des mots proposables : string array array, le dictionnaire dans lequel le mot mystère va être pioché : string array array 
	- précondition : les dictionnaires ne sont pas des tableaux vides
	- sortie : aucune
	- postcondition : la fonction tire aléatoirement le mot à trouver dans le second dictionnaire passé en paramètre,
	demande des mots de proposition à l'utilisateur, vérifie la bonne taille du mot, son abscence de caractères spéciaux, sa présence dans le premier dico passé en paramètre, affiche la grille de jeu motus en conséquence
	et vérifie si le joueur a gagné à chaque tour et agit en conséquence (fonction gagner si le joueur gagne, sinon s'il ne découvre pas le mot mystère fonction perdu )
	- lève une Assert_failure si les préconditions ne sont pas respectées
*)

val jouer : int -> string array array -> string array array -> unit


(** fonction debut
	- description : accueille l'utilisateur et lui demande les renseignements nécessaires pour jouer
	- entrée : dictionnaire des mots possibles : string array array, le dictionnaire dans lequel le mot mystère va être pioché : string array array
	- précondition : les dictionnaires ne sont pas vides
	- sortie : aucune
	- postcondition : demande au joueur le nombre d'essais qu'il souhaite avoir pour jouer, puis lui fait faire une partie,
	puis lui repropose de jouer à la fin de chaque partie en lui permettant de changer le nombre d'essais qu'il souhaite avoir pour découvrir le mot mystère	
	- lève une Assert_failure si les préconditions ne sont pas respectées
*)

val debut : string array array -> string array array -> unit

