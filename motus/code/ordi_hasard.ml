Random.self_init()

let partie_ordi_hasard (dictionnaire_pioche : string array array) : unit =
  (* fonction simulant une partie jouée par un ordinateur, qui jouerait des mots au hasard *)
  assert (Array.length dictionnaire_pioche <> 0);
  Affichage.logo();
  let pioche_mystere = Utilitaire.dico_choix.(Random.int Utilitaire.longueur_dico_choix) in (*le dico de choix est un string array array : on récupère d'abord un des tableaux de longueurs au hasard*)
  let mot_mystere = pioche_mystere.(Random.int (Array.length pioche_mystere)) in (*On choisit le mot mystère dans la petite base de donnée*)
  let longueur_mot_mystere = String.length mot_mystere in 
  
  print_endline "Combien d'essais voulez-vous donner à ce pauvre petit ordinateur ? (pas de trop, ni trop peu ! Entre 3 et 15 inclus disons (gare à vous si vous ne respectez pas))";
  let nombre_essais = ref (read_int ()) in (*On lit le nombre d'essais fixé par l'utilisateur*)
  assert ( !nombre_essais <= 15 && 3 <= !nombre_essais); (*On s'assure qu'il respecte bien ce que l'on veut*)

  
  let sous_tableau_affichage = Utilitaire.tableau_creation_affichage longueur_mot_mystere in (*nom trompeur : est en réalité un couple string * etat array *)
  let tableau_affichage = Array.make !nombre_essais sous_tableau_affichage in (*on initialise le tableau d'afficchage : on y ajoutera tous les essais et leur vérification, pour tout afficher à la fin*)
  let longueur_tableau_affichage = !nombre_essais in 
  
  let dico_bonne_taille = dictionnaire_pioche.(longueur_mot_mystere - Utilitaire.min_longueur) in (*dictionnaire_pioche étant un tableau de tableau, chacun contenant les mots de même taille, on récupère celui qui nous intéresse (ex si le mot mystère est de taille 5, les mots de longueur 5)*)
  let taille_dico = Array.length dico_bonne_taille in

  let essai = ref dico_bonne_taille.(Random.int (taille_dico)) in (*l'ordi réalise un premier essai en prenant un mot au hasard*)
  tableau_affichage.(0) <- (!essai,Utilitaire.verification !essai mot_mystere longueur_mot_mystere); (*On ajoute à l'affichage ce premier essai et sa vérification*)
  decr nombre_essais; (*l'ordi a utilisé un essai, on décrémente donc le nombre d'essais*)

  while !nombre_essais > 0 && !essai <> mot_mystere do (*tant qu'il a des essais et qu'il n'a pas trouvé le mot*)
    essai := dico_bonne_taille.(Random.int (taille_dico)); (*il refait un essai*)
    tableau_affichage.(longueur_tableau_affichage - !nombre_essais) <- (!essai,Utilitaire.verification !essai mot_mystere longueur_mot_mystere); (*on ajoute (dans l'ordre) les essais et leur verif*)
    decr nombre_essais; (*et on décrémente le nombre d'essais*)
  done; 

  Affichage.afficher_grille longueur_mot_mystere 0 tableau_affichage; (*on affiche la grille finale*)
  if !essai <> mot_mystere (*on détermine si l'ordi a gagné ou nom*)
  then 
    begin
      print_string "Quel dommage ! Le bot a perdu, il n'est pas très doué dis donc... Le mot mystère était : " ; 
      print_endline mot_mystere
    end
  else print_endline "Incroyable, mais vrai ! Le bot a gagné !!!"
