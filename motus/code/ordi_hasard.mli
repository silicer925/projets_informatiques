(** fonction partie_ordi_hasard
	- entrée : tableau de tableau de chaines de caractères
	- préconditions : Dans chaque sous_tableau, les chaînes ont la même taille et ces sous_tableaux sont rangés par ordre croissant de taille de chaînes, et le tableau en param n'est pas vide
	- sortie : unit
	- postconditions : pas d'effet de bords
    - Lève Assert_failure si le tableau en paramètre est vide
*)
val partie_ordi_hasard : string array array -> unit
