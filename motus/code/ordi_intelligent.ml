Random.self_init()

let occurence_string (mot : string) (lettre : char) : int = (*simple fonction retournant le nb d'occurence d'une lettre dans une chaîne*)
  let cpt = ref 0 in
  for i = 0 to (String.length mot) -1 do
    if mot.[i] = lettre then
      incr cpt
  done;
  !cpt
  
let rec occurence_liste (liste : char list) (lettre : char) (cpt : int) : int = (*simple fonction retournant le nb d'occurence d'une lettre dans une liste de lettres. Cpt doit être égal à 0 lors de l'appel*)
  match liste with
  | [] -> cpt
  | hd :: tl when hd = lettre -> occurence_liste tl lettre (cpt+1)
  | hd :: tl -> occurence_liste tl lettre cpt


exception Continue 
let note (dico : string array) (taille_dico : int) (mot : string) (verif : Utilitaire.etat array) (longueur_mot : int): int array =
  (* fonction permettant d'attribuer une "note" à un tableau de mot passé en paramètre. Chacun des mots se verra attribué un "1"
     ou un "0", en fonction de s'il est respectivement non compatible avec l'essai réalisé, ou compatible (ie 0 s'il est utile, 1 sinon)
     (NB : il sera souvent dit "on supprime du tableau" : par analogie avec la suite, cela signifie attribuer une note de 1 au mot)*)
  assert (Array.length dico = taille_dico && String.length mot = longueur_mot); (*n'ayant pas le mot mysère, on ne pas tester le fait que la vérification corresponde bien à celle du mot*)
  let tableau_notes = Array.make taille_dico 0 in (*on initialise le tableau des notes : au départ tous les mots sont bons*)
  for i = 0 to taille_dico -1 do (*on parcourt le dico en paramètre*)
    if dico.(i) = mot then (*on supprime du dico le mot que l'on vient de tester*)
      tableau_notes.(i) <- 1
    else
      try (* try/with permettant de gagner un peu de temps lors de l'exécution, avec l'exception Continue (si une des lettre du mot remplie une cdt de suppression, alors nul besoin de tester les autres)*)
        for j = 0 to longueur_mot -1 do (* on parcourt les lettres du mot (en paramètre, du dico, pas d'importance, ils font la même taille)*)
          if verif.(j) = Utilitaire.Bonne then (*Si on a une lettre bien placée, alors si le mot du dico n'a pas la même lettre au même endroit, on le supprime du dico*)
            begin 
              if dico.(i).[j] <> mot.[j] then 
                begin tableau_notes.(i) <- 1 ; raise Continue end 
            end
          else if verif.(j) = Utilitaire.Bof then (*Si une lettre est dans le mot mais n'est pas bien placée : alors on supprime les mots ayant cette lettre au même endroit, et ceux qui ne contiennent pas du tout cette lettre*)
            begin 
              if dico.(i).[j] = mot.[j] || not (String.contains dico.(i) mot.[j]) then
                begin tableau_notes.(i) <- 1 ; raise Continue end
            end
           else (*Cas le plus compliqué : si la lettre est mauvaise*)
            (* Plusieurs cas de figures se posent... Si la lettre n'est tout simplement pas dans le mot, ou si c'est une nouvelle occurence d'une lettre bien placée ou mal placée*)
            (* Afin de traiter au mieux ces cas de figures, nous devons d'abord récuperer les bonnes et bof lettres. Comme on ne sait pas combien il y en a, on fait cela dans une liste*)
            let liste_lettres_bonne_bof = ref [] in
            for k = 0 to longueur_mot -1 do
              if verif.(k) <> Utilitaire.Mauvaise then
                liste_lettres_bonne_bof := mot.[k] :: !liste_lettres_bonne_bof
            done;
            (*on va ensuite s'occuper en premier du second cas : si la lettre mauvaise se retrouve plus de fois dans le mot du dico que l'on regarde que dans les bonnes ou bof lettres, alors cela veut dire qu'elle est de trop et qu'il faut donc supprimer ce mot*)
            if occurence_string dico.(i) mot.[j] > occurence_liste !liste_lettres_bonne_bof mot.[j] 0 then
              begin tableau_notes.(i) <- 1 ; raise Continue end
            else
              (*Sinon, occupons-nous du premier cas : la lettre n'est pas dans le mot. Alors dans ce cas, il faut retirer tous les mots qui la contiennent... En faisant attention de ne pas supprimer ceux qui la contiennent déjà à une place bonne ou bof ! *)
              let chaine_tmp = ref "" in (*pour cela on crée une nouvelle chaîne, dans laquelle on ne met que les lettres bof ou mauvaise, sans que celle-ci n'apparaissent dans la liste des bonnes ou mauvaises lettre*)
              for k = 0 to longueur_mot -1 do
                if verif.(k) <> Utilitaire.Bonne && not (List.mem dico.(i).[k] !liste_lettres_bonne_bof) then
                  begin chaine_tmp := !chaine_tmp ^ (String.make 1 dico.(i).[k]) end
              done; 
              (*enfin, on regarde si cette nouvelle chaîne contient la lettre mauvaise : si oui, on supprime le mot*)
              if String.contains !chaine_tmp mot.[j] then
                begin tableau_notes.(i) <- 1 ; raise Continue end
        done;
      with Continue -> ()
  done;
  tableau_notes
  
  
let decoupage_dico (dico : string array) (taille_dico : int) (mot : string) (verif : Utilitaire.etat array) (longueur_mot : int): string array = 
  (*fonction qui va venir "découper" (entendre réduire) un tableau de chaîne de carcatères, en ne gardant que celle dont la note est 0*)
  (*Comme on ne sait pas à l'avance combien de mots vont vérifier cette condition, on utilise une liste.*)
  assert (Array.length dico = taille_dico && String.length mot = longueur_mot); (*n'ayant pas le mot mysère, on ne pas tester le fait que la vérification corresponde bien à celle du mot*)
  let tableau_notes = note dico taille_dico mot verif longueur_mot in (*on récupère le tableau des notes du dico*)
  let liste_tmp = ref [] in
  for i = 0 to taille_dico -1 do (*pour chaque mot, si sa note est 0, alors on le garde et donc on le met dans la liste créée*)
    if tableau_notes.(i) = 0 then 
      liste_tmp := dico.(i) :: !liste_tmp
  done;
  Array.of_list !liste_tmp (*On transforme la liste en tableau, afin de pouvoir continuer à le manipuler par la suite*)

let partie_ordi_intelligent (dictionnaire_pioche : string array array) : unit =
  (*fonction simulant une partie de motus jouée par un ordinateur plutôt intelligent : il se souvient des coups précédents et réduit sans cesse le champ des possibilités en réduisant le tableau dans lequel il fait ses essais*)
  assert (Array.length dictionnaire_pioche <> 0);
  Affichage.logo();
  let pioche_mystere = Utilitaire.dico_choix.(Random.int Utilitaire.longueur_dico_choix) in (*le dico de choix est un string array array : on récupère d'abord un des tableaux de longueurs au hasard*)
  let mot_mystere = pioche_mystere.(Random.int (Array.length pioche_mystere)) in (*On choisit le mot mystère dans la petite base de donnée*)
  let longueur_mot_mystere = String.length mot_mystere in 

  print_endline "Combien d'essais voulez-vous donner à ce magnifique ordinateur ? (pas de trop, ni trop peu ! Entre 3 et 15 inclus disons (gare à vous si vous ne respectez pas))";
  print_endline "(il est conseillé de donner entre 6 et 9 essais, en dessous étant trop peu, au dessus prenant de la place dans l'affichage à rien)";
  (*Il est conseillé d'en donner autant, dans le cas où le bot n'aurait vraiment pas de chance et tomberait sur des cas pathologiques, exemple : Serie/Serge/Serre/Serbe/Serpe etc*)
  let nombre_essais = ref (read_int ()) in (*On lit le nombre d'essais fixé par l'utilisateur*)
  assert ( !nombre_essais <= 15 && 3 <= !nombre_essais); (*On s'assure qu'il respecte bien ce que l'on veut*)

  let sous_tableau_affichage = Utilitaire.tableau_creation_affichage longueur_mot_mystere in (*de même que pour le bot non-intelligent*)
  let tableau_affichage = Array.make !nombre_essais sous_tableau_affichage in
  let longueur_tableau_affichage = !nombre_essais in
  
  let dico_bonne_taille = ref dictionnaire_pioche.(longueur_mot_mystere - Utilitaire.min_longueur) in (*de même, on récupère le dico avec les bonne taille. Cette fois-ci, on en fait une référence, car il évoluera (diminuera) au fur et à mesure des découpages*)
  let essai = ref !dico_bonne_taille.(Random.int (Array.length !dico_bonne_taille)) in (*il fait un essai, et l'ajoute après au tableau d'affichage*)
  let verification_mot = ref (Utilitaire.verification !essai mot_mystere longueur_mot_mystere) in (*on vérifie l'essai (on le met dans une varibale car on s'en sert plusieurs fois dans la boucle)*)
  tableau_affichage.(0) <- (!essai,!verification_mot);
  decr nombre_essais;

  while !nombre_essais > 0 && !essai <> mot_mystere do (*tant qu'il reste des essais ou qu'il n'a pas trouvé le mot*)
    dico_bonne_taille := decoupage_dico !dico_bonne_taille (Array.length !dico_bonne_taille) !essai !verification_mot longueur_mot_mystere; (*on découpe le dico actuel*)
    essai := !dico_bonne_taille.(Random.int (Array.length !dico_bonne_taille)); (*on refait un nouvel essai*)
    verification_mot := Utilitaire.verification !essai mot_mystere longueur_mot_mystere; 
    tableau_affichage.(longueur_tableau_affichage - !nombre_essais) <- (!essai,!verification_mot);(*on ajoute au tableau d'affichage*)
    decr nombre_essais; (*il perd un essai*)
  done; 
  Affichage.afficher_grille longueur_mot_mystere 0 tableau_affichage;(*on affiche la grille finale*)
  if !essai <> mot_mystere 
  then 
    begin 
      print_string "COMMENT ??? Ce n'est pas possible, je n'ai pas pu perdre... Le mot mystère était : " ; 
      print_endline mot_mystere
    end
  else print_endline "Hahaha, trivial comme dirait l'autre, encore une victoire pour moi !"
