(** fonction occurence_string
	- entrée : une chaîne de caractères et un caractère
	- préconditions : aucune
	- sortie : un entier
	- postconditions : l'entier renvoyé correspond au nb d'occurence du caractère dans la chaîne, aucun effet de bord
*)
val occurence_string : string -> char -> int

(** fonction occurence_liste
	- entrée : liste de caractères, un caractère et un entier
	- préconditions : l'entier est égal à 0 à l'appel de la fct
	- sortie : entier
	- postconditions : l'entier renvoyé correspond au nb d'occurence du caractère dans la liste, aucun effet de bord
*)
val occurence_liste : char list-> char -> int -> int

(** fonction note
	- entrée : un tableau de chaines de caractère, sa taille, une chaine de caractère, un tableau d'états, et la taille du mot 
	- préconditions : tout correspond bien (les tailles aux objets, le tableau d'états au mot en paramètre)
	- sortie : tableau d'entiers
	- postconditions : à chaque mot est assimilé un chiffre, 1 ou 0, 0 s'il est compatible avec le mot recherché, 1 sinon, pas d'effets de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées (sauf pour le fait que verif correspondent à la vérification du mot)
*)
val note : string array -> int -> string -> Utilitaire.etat array -> int -> int array

(** fonction decoupage_dico
	- entrée : un tableau de chaines de caractère, sa taille, une chaine de caractère, un tableau d'états, et la taille du mot 
	- préconditions : tout correspond bien (les tailles aux objets, le tableau d'états au mot en paramètre)
	- sortie : tableau de chaînes de caractères
	- postconditions : renvoie le dictionnaire d'origine privé des mots non compatibles avec le mot recherché, pas d'effets de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées (sauf pour le fait que verif correspondent à la vérification du mot)
*)
val decoupage_dico : string array -> int -> string -> Utilitaire.etat array -> int -> string array


(** fonction partie_ordi_intelligent
	- entrée : tableau de tableaux de chaines de caractères
	- préconditions : Dans chaque sous_tableau, les chaînes ont la même taille et ces sous_tableaux sont rangés par ordre croissant de taille de chaînes, et le tableau en param n'est pas vide
	- sortie : unit
	- postconditions : pas d'effets de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val partie_ordi_intelligent : string array array -> unit

