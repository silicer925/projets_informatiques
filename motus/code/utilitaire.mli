(** type etat (type somme)
	- représente l'état d'une lettre
*)
type etat = Mauvaise | Bof | Bonne

(** variable min_longueur 
    - représente la longueur minimale des mots utilisés
    *)
val min_longueur : int

(** variable max_longueur 
    - représente la longueur maximale des mots utilisés
    *)
val max_longueur : int

(** variable dico_choix
	- Dictionnaire des mots utilisés comme mots mystère pour le motus
*)
val dico_choix : string array array

(** variable longueur_dico_choix 
    - représente la taille du dictionnaire de choix : dico_choix
    *)
val longueur_dico_choix : int

(** variable lexique
	- Dictionnaire des mots existants de la langue française
*)
val lexique : string array array

(** fonction verification
	- entrée : deux chaine de caractères (le mot à vérifier et le mot mystère), et la longueur du mot mystere
	- précondition : mots de la même taille, la taille correspond bien à la longueur du mot
	- sortie : tableau d'états 
	- postcondition : tableau de la même taille que les mots, et qui à chaque lettre associe son état, état correspondant à si la lettre est bien placée, mal placée, ou plus/pas existante, pas d'effets de bord
    - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val verification : string -> string -> int -> etat array

(** fonction tableau_creation_affichage
	- entrée : un entier
	- préconditions : aucune
	- sortie : une couple d'une chaîne de caractères et d'un tableau d'états
	- postconditions : Chaine de longueur l'entier en paramètre constituée d'espaces, et un tableau d'états "Mauvaise" (de longueur l'entier passé en param), pas d'effets de bord
*)
val tableau_creation_affichage : int -> string * etat array
